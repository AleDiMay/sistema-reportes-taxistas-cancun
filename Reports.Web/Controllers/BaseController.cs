﻿using System.Web.Mvc;
using Reports.Application.Filters;

namespace Reports.Application.Controllers
{
    [UserValidatorPermission]
    public class BaseLoggedController : BaseController
    {
    }

    public class BaseController : Controller
    {
        public new RedirectToRouteResult RedirectToAction(string action, string controller, object routeValues = null)
        {
            return base.RedirectToAction(action, controller, routeValues);
        }
    }
}