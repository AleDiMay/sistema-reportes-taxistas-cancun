﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CommonService;
using CommonService.Models;
using Enums = Reports.Models.Enums;

namespace Reports.API.Helpers
{
    public static class SiteOptions
    {
        public static string GetReportStatusText(int? status)
        {
            var message = string.Empty;
            switch (status)
            {
                case 0:
                    message = "Sin leer";
                    break;
                case 1:
                    message = "En proceso";
                    break;
                default:
                    message = string.Empty;
                    break;
            }
            return message;
        }

        public static string GetCSSClassByStatus(int? status)
        {
            var message = string.Empty;
            switch (status)
            {
                case 0:
                    message = "list-group-item-warning";
                    break;
                case 1:
                    message = "list-group-item-info";
                    break;
                default:
                    message = string.Empty;
                    break;
            }
            return message;
        }

    }
}