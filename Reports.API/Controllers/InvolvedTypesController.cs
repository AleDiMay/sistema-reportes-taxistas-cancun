﻿using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Legendary.Core.Model;
using Legendary.Core.Utils;
using Reports.Models;
using Reports.Models.Requests;

namespace Reports.API.Controllers
{
    public class InvolvedTypesController : BaseController
    {
        [UserService.EncryptedApiAuthorization(
            (int) SecurityService.Model.SystemPermissions.ReportsPermissions.ViewInvolvedType)]
        public IQueryable<InvolvedType> GetInvolvedTypes()
        {
            return db.InvolvedTypes;
        }


        [UserService.EncryptedApiAuthorization(
            (int) SecurityService.Model.SystemPermissions.ReportsPermissions.ViewInvolvedType)]
        [ResponseType(typeof (Paginable<InvolvedType>))]
        public async Task<IHttpActionResult> GetInvolvedType(string request)
        {
            var finalRequest = request.UrlDecode<InvolvedTypeRequest>();
            var types = db.InvolvedTypes;
            var results = await finalRequest.ExecuteRequestAsync(types);
            var paginated = results.Paginate(finalRequest);
            return Ok(paginated);
        }


        [UserService.EncryptedApiAuthorization(
            (int) SecurityService.Model.SystemPermissions.ReportsPermissions.ViewInvolvedType)]
        [ResponseType(typeof (InvolvedType))]
        public async Task<IHttpActionResult> GetInvolvedType(int id)
        {
            InvolvedType involvedType = await db.InvolvedTypes.FindAsync(id);
            if (involvedType == null)
            {
                return NotFound();
            }
            return Ok(involvedType);
        }


        [UserService.EncryptedApiAuthorization(
            (int) SecurityService.Model.SystemPermissions.ReportsPermissions.EditInvolvedType)]
        [ResponseType(typeof (void))]
        public async Task<IHttpActionResult> PutInvolvedType(int id, InvolvedType involvedType)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != involvedType.Id)
            {
                return BadRequest();
            }

            db.Entry(involvedType).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!InvolvedTypeExists(id))
                {
                    return NotFound();
                }
                throw;
            }

            return StatusCode(HttpStatusCode.NoContent);
        }


        [UserService.EncryptedApiAuthorization(
            (int) SecurityService.Model.SystemPermissions.ReportsPermissions.CreateInvolvedType)]
        [ResponseType(typeof (InvolvedType))]
        public async Task<IHttpActionResult> PostInvolvedType(InvolvedType involvedType)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.InvolvedTypes.Add(involvedType);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new {id = involvedType.Id}, involvedType);
        }


        [UserService.EncryptedApiAuthorization(
            (int) SecurityService.Model.SystemPermissions.ReportsPermissions.DeleteInvolvedType)]
        [ResponseType(typeof (InvolvedType))]
        public async Task<IHttpActionResult> DeleteInvolvedType(int id)
        {
            InvolvedType involvedType = await db.InvolvedTypes.FindAsync(id);
            if (involvedType == null)
            {
                return NotFound();
            }

            db.InvolvedTypes.Remove(involvedType);
            await db.SaveChangesAsync();

            return Ok(involvedType);
        }

        [ResponseType(typeof (InvolvedType))]
        public IHttpActionResult Options()
        {
            return Ok(new InvolvedType());
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }


        private bool InvolvedTypeExists(int id)
        {
            return db.InvolvedTypes.Count(e => e.Id == id) > 0;
        }
    }
}