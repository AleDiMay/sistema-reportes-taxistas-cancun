﻿using System.Collections.Generic;

namespace Reports.Models
{
    public class Enums
    {
        public enum ReportsStatus
        {
            None = 0,
            Viewed = 1,
            InFollowing = 2,
            Closed = 3,
            Expiring = 4,
            Expired = 5
        }
        public enum ReportPriority
        {
            Urgent = 0,
            High= 1,
            Regular = 2,
            Low = 3
        }


        public enum ReportType
        {
            None = 0,
            BadRate = 1,
            ForgottenItem = 2,
            CarCrash = 3,
            BadDriverBehavior = 4,
        }

        public enum SuggestionType
        {
            None = 0,
            Congratulation = 1,
            Suggestion = 2
        }

        public static Dictionary<ReportType, string> ReportDictionary = new Dictionary<ReportType, string>
        {
            {ReportType.None, "Ninguna."},
            {ReportType.BadRate, "Tarifa incorrecta."},
            {ReportType.ForgottenItem, "Objeto olvidado."},
            {ReportType.CarCrash, "Accidente automovilístico"},
            {ReportType.BadDriverBehavior, "Problemas con el taxista."}
        };

        public static Dictionary<SuggestionType, string> SuggestionDictionary = new Dictionary<SuggestionType, string>
        {
            {SuggestionType.None, "Ninguno."},
            {SuggestionType.Congratulation, "Felicitación."},
            {SuggestionType.Suggestion, "Sugerencia."}
        };
    }
}