# Sistema de Reportes del Sindicato de Taxistas Cancun #

Aplicación Web de Administración de "Incidencias", orientado a proporcionar seguimiento amigable y medible de cada uno de los reportes ingresados por diversos nodos.

### ¿De que es capaz la aplicación? ###

* Creación, edición, y seguimiento de Incidencias.
* Jerarquias de Privilegios de Usuario por Grupo o de Manera Individual.
* Graficas, Reportes y Resumenes Exportables en Distintos Formatos.
* Completa Integración dentro del ecosistema de software (Padron, Padrón Vehicular, Manejo de Ingresos, Etc).

### Tecnologías Aplicadas ###

* C#
* Linq
* Razor MVC
* SQL Databases
* Implementación Azure