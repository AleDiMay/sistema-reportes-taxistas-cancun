﻿using CommonService.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Reports.Application.Controllers
{
    public class LoginController : BaseLoginController
    {
        public LoginController()
            : base("Login", "Login", "Index", "Home") { }
    }
}
