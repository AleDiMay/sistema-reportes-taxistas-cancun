﻿using System.Web.Mvc;
using UserService.Model;
using Legendary.Core.Utils;
using CommonService.Controllers;

namespace Reports.Application.Controllers
{
    public class AccountController : BaseLoginController
    {
        public AccountController()
            : base("Login", "Login", "Index", "Home") { }
    }
}