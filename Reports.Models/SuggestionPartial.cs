﻿using System.ComponentModel.DataAnnotations;

namespace Reports.Models
{
    [MetadataType(typeof(SuggestionMD))]
    public partial class Suggestion
    {
    }

    public class SuggestionMD
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "El email no puede estar vacío")]
        [EmailAddress(ErrorMessage = "El email no es válido")]
        public string Email { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "El nombre es necesario")]
        public string Name { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Un mensaje es necesario")]
        [StringLength(1200, ErrorMessage = "El mensaje no puede tener mas de 1200 caracteres.")]
        public string Description { get; set; }
    }
}