﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Legendary.Core.Model;
using Reports.Models;
using Reports.Models.Requests;

namespace Reports.Client
{
    public class GeneralReportServices
    {
        private readonly AllocationDepartmentServices _allocationDepartmentServices;
        private readonly AllocationDepartmentUserService _allocationDepartmentUserService;
        private readonly ReportService _reportService;
        private readonly EditionsServices _editionsServices;
        private readonly SuggestionServices _suggestionServices;
        private readonly MessageSourcesClient _messageSourcesClient;
        private readonly ReportTypesService _reportTypesService;
        private readonly SuggestionTypesService _suggestionTypesService;
        private readonly InvolvedPersonServices _involvedPersonServices;
        private readonly InvolvedTypesServices _involvedTypesServices;


        public GeneralReportServices(string apiUri)
        {
            _allocationDepartmentServices = new AllocationDepartmentServices(apiUri);
            _allocationDepartmentUserService = new AllocationDepartmentUserService(apiUri);
            _reportService = new ReportService(apiUri);
            _editionsServices = new EditionsServices(apiUri);
            _suggestionServices = new SuggestionServices(apiUri);
            _messageSourcesClient = new MessageSourcesClient(apiUri);
            _reportTypesService = new ReportTypesService(apiUri);
            _suggestionTypesService = new SuggestionTypesService(apiUri);
            _involvedPersonServices = new InvolvedPersonServices(apiUri);
            _involvedTypesServices = new InvolvedTypesServices(apiUri);
        }



        #region AllocationDepartment

        public IEnumerable<AllocationDepartment> GetAllocationDepartments()
        {
            return _allocationDepartmentServices.Get();
        }

        public Paginable<AllocationDepartment> GetAllocationDepartment(AllocationDepartmentRequest request)
        {
            return _allocationDepartmentServices.Get(request);
        }

        public AllocationDepartment GetAllocationDepartment(int? id)
        {
            if (id.HasValue)
                return _allocationDepartmentServices.Get(id.Value);

            throw new NullReferenceException("id");
        }

        public AllocationDepartment CreateAllocationDepartment(AllocationDepartment allocationDepartment)
        {
            return _allocationDepartmentServices.Save(allocationDepartment);
        }

        public AllocationDepartment UpdateAllocationDepartment(AllocationDepartment allocationDepartment)
        {
            return _allocationDepartmentServices.Update(allocationDepartment.Id, allocationDepartment);
        }


        #endregion


        #region AllocationDepartmentUser

        public IEnumerable<AllocationDepartmentUser> GetAllocationDepartmentUsers()
        {
            return _allocationDepartmentUserService.Get();
        }

        public Paginable<AllocationDepartmentUser> GetAllocationDepartmentUser(AllocationDepartmentUserRequest request)
        {
            return _allocationDepartmentUserService.Get(request);
        }

        public AllocationDepartmentUser GetAllocationDepartmentUser(int? id)
        {
            if (id.HasValue)
                return _allocationDepartmentUserService.Get(id.Value);

            throw new NullReferenceException("id");
        }

        public AllocationDepartmentUser CreateAllocationDepartmentUser(AllocationDepartmentUser allocationDepartmentUser)
        {
            return _allocationDepartmentUserService.Save(allocationDepartmentUser);
        }

        public AllocationDepartmentUser UpdateAllocationDepartmentUser(AllocationDepartmentUser allocationDepartmentUser)
        {
            return _allocationDepartmentUserService.Update(allocationDepartmentUser.Id, allocationDepartmentUser);
        }


        #endregion


        #region Report

        public IEnumerable<Report> GetReports()
        {
            return _reportService.Get();
        }

        public Paginable<Report> GetReport(ReportsRequest request)
        {
            return _reportService.Get(request);
        }

        public Report GetReport(int? id)
        {
            if (id.HasValue)
                return _reportService.Get(id.Value);

            throw new NullReferenceException("id");
        }

        public Report CreateReport(Report report)
        {
            return _reportService.Save(report);
        }

        public Report UpdateReport(Report report)
        {
            return _reportService.Update(report.Id, report);
        }

        #endregion


        #region Edition

        public IEnumerable<Edition> GetEditions()
        {
            return _editionsServices.Get();
        }

        public Paginable<Edition> GetEdition(EditionRequest request)
        {
            return _editionsServices.Get(request);
        }

        public Edition GetEdition(int? id)
        {
            if (id.HasValue)
                return _editionsServices.Get(id.Value);

            throw new NullReferenceException("id");
        }

        public Edition CreateEdition(Edition edition)
        {
            return _editionsServices.Save(edition);
        }

        public Edition UpdateEdition(Edition edition)
        {
            return _editionsServices.Update(edition.Id, edition);
        }

        #endregion


        #region Suggestion

        public IEnumerable<Suggestion> GetSuggestions()
        {
            return _suggestionServices.Get();
        }

        public Paginable<Suggestion> GetSuggestion(SuggestionRequest request)
        {
            return _suggestionServices.Get(request);
        }

        public Suggestion GetSuggestion(int? id)
        {
            if (id.HasValue)
                return _suggestionServices.Get(id.Value);

            throw new NullReferenceException("id");
        }

        public Suggestion CreateSuggestion(Suggestion suggestion)
        {
            return _suggestionServices.Save(suggestion);
        }

        public Suggestion UpdateSuggestion(Suggestion suggestion)
        {
            return _suggestionServices.Update(suggestion.Id, suggestion);
        }



        #endregion


        #region MessageSources

        public IEnumerable<MessageSource> GetMessageSources()
        {
            return _messageSourcesClient.Get();
        }

        public Paginable<MessageSource> GetMessageSource(MessageSourceRequest request)
        {
            return _messageSourcesClient.Get(request);
        }

        public MessageSource GetMessageSource(int? id)
        {
            if (id.HasValue)
                return _messageSourcesClient.Get(id.Value);

            throw new NullReferenceException("id");
        }

        public MessageSource CreateMessageSource(MessageSource messageSource)
        {
            return _messageSourcesClient.Save(messageSource);
        }

        public MessageSource UpdateMessageSource(MessageSource messageSource)
        {
            return _messageSourcesClient.Update(messageSource.Id, messageSource);
        }


        #endregion


        #region ReportType

        public IEnumerable<ReportType> GetReportTypes()
        {
            return _reportTypesService.Get();
        }

        public Paginable<ReportType> GetReport(ReportTypeRequest request)
        {
            return _reportTypesService.Get(request);
        }

        public ReportType GetReportType(int? id)
        {
            if (id.HasValue)
                return _reportTypesService.Get(id.Value);

            throw new NullReferenceException("id");
        }

        public ReportType CreateReportType(ReportType reportType)
        {
            return _reportTypesService.Save(reportType);
        }

        public ReportType UpdateReportType(ReportType reportType)
        {
            return _reportTypesService.Update(reportType.Id, reportType);
        }


        #endregion


        #region SuggestionType

        public IEnumerable<SuggestionType> GetSuggestionTypes()
        {
            return _suggestionTypesService.Get();
        }

        public Paginable<SuggestionType> GetReport(SuggestionTypeRequest request)
        {
            return _suggestionTypesService.Get(request);
        }

        public SuggestionType GetSuggestionType(int? id)
        {
            if (id.HasValue)
                return _suggestionTypesService.Get(id.Value);

            throw new NullReferenceException("id");
        }

        public SuggestionType CreateSuggestionType(SuggestionType suggestionType)
        {
            return _suggestionTypesService.Save(suggestionType);
        }

        public SuggestionType UpdateSuggestionType(SuggestionType suggestionType)
        {
            return _suggestionTypesService.Update(suggestionType.Id, suggestionType);
        }

        #endregion


        #region InvolvedPerson

        public IEnumerable<InvolvedPerson> GetInvolvedPersons()
        {
            return _involvedPersonServices.Get();
        }

        public Paginable<InvolvedPerson> GetInvolvedPerson(InvolvedPersonRequest request)
        {
            return _involvedPersonServices.Get(request);
        }

        public InvolvedPerson GetInvolvedPerson(int? id)
        {
            if (id.HasValue)
                return _involvedPersonServices.Get(id.Value);

            throw new NullReferenceException("id");
        }

        public InvolvedPerson CreateInvolvedPerson(InvolvedPerson involvedPerson)
        {
            return _involvedPersonServices.Save(involvedPerson);
        }

        public InvolvedPerson UpdateInvolvedPerson(InvolvedPerson involvedPerson)
        {
            return _involvedPersonServices.Update(involvedPerson.Id, involvedPerson);
        }

        #endregion


        #region InvolvedType

        public IEnumerable<InvolvedType> GetInvolvedTypes()
        {
            return _involvedTypesServices.Get();
        }

        public Paginable<InvolvedType> GetInvolvedType(InvolvedTypeRequest request)
        {
            return _involvedTypesServices.Get(request);
        }

        public InvolvedType GetInvolvedType(int? id)
        {
            if (id.HasValue)
                return _involvedTypesServices.Get(id.Value);

            throw new NullReferenceException("id");
        }

        public InvolvedType CreateInvolvedType(InvolvedType involvedType)
        {
            return _involvedTypesServices.Save(involvedType);
        }

        public InvolvedType UpdateInvolvedType(InvolvedType involvedType)
        {
            return _involvedTypesServices.Update(involvedType.Id, involvedType);
        }

        #endregion


    }
}
