﻿using System.Web.Http;
using UserService;

namespace Reports.API
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services
            config.EnableCors();
            CommonService.Helpers.WebApiHelper.InitializeStandardWebApiConfigurations(config, new EncryptedApiAuthentication());
            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }
    }
}
