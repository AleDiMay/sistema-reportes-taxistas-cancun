﻿using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Reports.Models;

namespace Reports.API.Controllers
{
    public class EditionsController : BaseController
    {
        [UserService.EncryptedApiAuthorization(
            (int) SecurityService.Model.SystemPermissions.ReportsPermissions.ViewEdition)]
        public IQueryable<Edition> GetEditions()
        {
            return db.Editions;
        }

        // GET: api/Editions/5
        [ResponseType(typeof (Edition))]
        [UserService.EncryptedApiAuthorization(
            (int) SecurityService.Model.SystemPermissions.ReportsPermissions.ViewEdition)]
        public async Task<IHttpActionResult> GetEdition(int id)
        {
            Edition edition = await db.Editions.FindAsync(id);
            if (edition == null)
            {
                return NotFound();
            }

            return Ok(edition);
        }


        // POST: api/Editions
        [ResponseType(typeof (Edition))]
        [UserService.EncryptedApiAuthorization(
            (int) SecurityService.Model.SystemPermissions.ReportsPermissions.CreateEdition)]
        public async Task<IHttpActionResult> PostEdition(Edition edition)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Editions.Add(edition);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new {id = edition.Id}, edition);
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool EditionExists(int id)
        {
            return db.Editions.Count(e => e.Id == id) > 0;
        }
    }
}