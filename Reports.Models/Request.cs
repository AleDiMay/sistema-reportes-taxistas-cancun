﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Legendary.Core.Model.Request;
using Legendary.Core.Utils;

namespace Reports.Models
{
    public class ReportsRequest : PaginatedRequest<Report>
    {
        public int? Status { get; set; }
        public int? CarNumber { get; set; }
        public DateTime? MinDate { set; get; }
        public DateTime? MaxDate { set; get; }
        public int? ModifiedByUserId { get; set; }
        public int? SourceId { get; set; }
        public string Email { get; set; }
        public int? Type { get; set; }
        public bool GetAllReportsInOnePage { get; set; }
        public string DocumentReferenceNumber { get; set; }
        public bool WithFullData { get; set; }

        public override IQueryable<Report> ExecuteRequest(IQueryable<Report> original)
        {
            original = original.OrderByDescending(a => a.UtcDateCreated);
            if (Email.IsNotEmptyOrNull())
                original = original.Where(s => !(s.Email == null || s.Email.Trim() == string.Empty) && s.Email == Email);
            if (CarNumber.IsNotEmptyOrNull())
                original = original.Where(s => s.CarNumber == CarNumber);
            if (ModifiedByUserId.IsNotEmptyOrNull())
                original = original.Where(s => s.ModifiedByUserId == ModifiedByUserId);
            if (DocumentReferenceNumber.IsNotEmptyOrNull())
                original = original.Where(s => s.DocumentReferenceNumber == DocumentReferenceNumber);

            if (SourceId.IsNotEmptyOrNull())
            {
                if (SourceId > 0)
                {
                    original = original.Where(s => s.SourceId == SourceId);
                }
            }
            if (Type.IsNotEmptyOrNull())
            {
                if (Type > 0)
                {
                    original = original.Where(s => s.Type == Type);
                }
            }
            if (MinDate.IsNotEmptyOrNull())
                original = original.Where(s => DbFunctions.TruncateTime(s.UtcDateCreated) >= DbFunctions.TruncateTime(MinDate));
            if (MaxDate.IsNotEmptyOrNull())
                original = original.Where(s => DbFunctions.TruncateTime(s.UtcDateCreated) <= DbFunctions.TruncateTime(MaxDate));
            return original;
        }

        public IEnumerable<Report> ExecuteNoEntityRequest(IEnumerable<Report> original)
        {
            if (MinDate.IsNotEmptyOrNull())
                original = original.Where(s => s.UtcDateCreated.Date >= MinDate);
            if (MaxDate.IsNotEmptyOrNull())
                original = original.Where(s => s.UtcDateCreated.Date <= MaxDate);
            return original;
        }


    }


    public class AllocationDepartmentRequest : PaginatedRequest<AllocationDepartment>
    {
        public int? Id { get; set; }

        public string Name { get; set; }

        public int? AllowedDepartment { get; set; }

        public override IQueryable<AllocationDepartment> ExecuteRequest(IQueryable<AllocationDepartment> original)
        {
            if (Id.IsNotEmptyOrNull() && Id > 0)
                original = original.Where(c => c.Id == Id);

            if (Name.IsNotEmptyOrNull())
                original = original.Where(c => c.Name == Name);

            if (AllowedDepartment.IsNotEmptyOrNull() && AllowedDepartment > 0)
                original = original.Where(c => !string.IsNullOrEmpty(c.AvailableAllocationDepartments) && c.AvailableAllocationDepartments.Contains(AllowedDepartment.ToString()) );

            return original;
        }
    }

    public class AllocationDepartmentUserRequest : PaginatedRequest<AllocationDepartmentUser>
    {

        public int? AllocationDepartmentId { get; set; }

        public int? UserId { get; set; }

        public override IQueryable<AllocationDepartmentUser> ExecuteRequest(IQueryable<AllocationDepartmentUser> original)
        {
            if (AllocationDepartmentId.IsNotEmptyOrNull() && AllocationDepartmentId > 0)
                original = original.Where(c => c.AllocationDepartmentId == AllocationDepartmentId);
            if (UserId.IsNotEmptyOrNull() && UserId > 0)
                original = original.Where(c => c.UserId == UserId);

            return original;
        }
    }


    public class EditionRequest : PaginatedRequest<Edition>
    {
        public int EditedById { get; set; }

        public int ReportId { get; set; }

        public override IQueryable<Edition> ExecuteRequest(IQueryable<Edition> original)
        {
            if (EditedById.IsNotEmptyOrNull() && EditedById > 0)
                original = original.Where(c => c.EditedById == EditedById);
            if (ReportId.IsNotEmptyOrNull() && ReportId > 0)
                original = original.Where(c => c.ReportId == ReportId);
            return original;
        }
    }

    public class SuggestionRequest : PaginatedRequest<Suggestion>
    {
        public int UserId { get; set; }

        public int Type { get; set; }

        public int Status { get; set; }

        public int SourceId { get; set; }

        public override IQueryable<Suggestion> ExecuteRequest(IQueryable<Suggestion> original)
        {
            if (UserId.IsNotEmptyOrNull() && UserId > 0)
                original = original.Where(c => c.UserId == UserId);
            if (Type.IsNotEmptyOrNull() && Type > 0)
                original = original.Where(c => c.Type == Type);
            if (Status.IsNotEmptyOrNull() && Status > 0)
                original = original.Where(c => c.Status == Status);
            if (SourceId.IsNotEmptyOrNull() && SourceId > 0)
                original = original.Where(c => c.SourceId == SourceId);
            return original;
        }
    }

    public class ReportTypeRequest : PaginatedRequest<ReportType>
    {
        public int Id { get; set; }

        public override IQueryable<ReportType> ExecuteRequest(IQueryable<ReportType> original)
        {
            if (Id.IsNotEmptyOrNull() && Id > 0)
                original = original.Where(c => c.Id == Id);
            return original;
        }
    }


    public class SuggestionTypeRequest : PaginatedRequest<SuggestionType>
    {
        public int Id { get; set; }

        public override IQueryable<SuggestionType> ExecuteRequest(IQueryable<SuggestionType> original)
        {
            if (Id.IsNotEmptyOrNull() && Id > 0)
                original = original.Where(c => c.Id == Id);
            return original;
        }
    }


    public class MessageSourceRequest : PaginatedRequest<MessageSource>
    {
        public int Id { get; set; }
        public override IQueryable<MessageSource> ExecuteRequest(IQueryable<MessageSource> original)
        {
            if (Id.IsNotEmptyOrNull() && Id > 0)
                original = original.Where(c => c.Id == Id);
            return original;
        }
    }

    public class InvolvedPersonRequest : PaginatedRequest<InvolvedPerson>
    {
        public int ReportId { get; set; }

        public int CreatedByUserId { get; set; }

        public int InvolvedTypeId { get; set; }


        public override IQueryable<InvolvedPerson> ExecuteRequest(IQueryable<InvolvedPerson> original)
        {
            if (ReportId.IsNotEmptyOrNull() && ReportId > 0)
                original = original.Where(c => c.ReportId == ReportId);
            if (CreatedByUserId.IsNotEmptyOrNull() && CreatedByUserId > 0)
                original = original.Where(c => c.CreatedByUserId == CreatedByUserId);
            if (InvolvedTypeId.IsNotEmptyOrNull() && InvolvedTypeId > 0)
                original = original.Where(c => c.InvolvedTypeId == InvolvedTypeId);
            return original;
        }
    }


    public class InvolvedTypeRequest : PaginatedRequest<InvolvedType>
    {
        public int Id { get; set; }
        public override IQueryable<InvolvedType> ExecuteRequest(IQueryable<InvolvedType> original)
        {
            if (Id.IsNotEmptyOrNull() && Id > 0)
                original = original.Where(c => c.Id == Id);
            return original;
        }
    }

}
