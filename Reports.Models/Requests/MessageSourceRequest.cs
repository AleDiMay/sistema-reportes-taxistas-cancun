﻿using System.Linq;
using Legendary.Core.Model.Request;
using Legendary.Core.Utils;

namespace Reports.Models.Requests
{
    public class MessageSourceRequest : PaginatedRequest<MessageSource>
    {
        public int Id { get; set; }

        public override IQueryable<MessageSource> ExecuteRequest(IQueryable<MessageSource> original)
        {
            if (Id.IsNotEmptyOrNull() && Id > 0)
                original = original.Where(c => c.Id == Id);
            return original;
        }
    }
}