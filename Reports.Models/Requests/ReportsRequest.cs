﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Legendary.Core.Model.Request;
using Legendary.Core.Utils;

namespace Reports.Models.Requests
{
    public class ReportsRequest : PaginatedRequest<Report>
    {
        public int? Status { get; set; }
        public int? CarNumber { get; set; }
        public DateTime? MinDate { set; get; }
        public DateTime? MaxDate { set; get; }
        public int? ModifiedByUserId { get; set; }
        public int? SourceId { get; set; }
        public string Email { get; set; }
        public int? Type { get; set; }
        public bool GetAllReportsInOnePage { get; set; }
        public int? Id { get; set; }
        public bool WithFullData { get; set; }
        public int? Priority { get; set; }
        //Tipo de Ordenado
        public int? OrderById { get; set; }
        public int? OrderByStatus { get; set; }
        public int? OrderByPriority { get; set; }

        //Relantioships Request
        public bool? GetMessageResource { get; set; }
        public bool? GetEditions { get; set; }
        public bool? GetInvolvedPersons { get; set; }

        public override IQueryable<Report> ExecuteRequest(IQueryable<Report> original)
        {
           
            if (Email.IsNotEmptyOrNull())
                original = original.Where(s => !(s.Email == null || s.Email.Trim() == string.Empty) && s.Email == Email);            
            if (CarNumber.IsNotEmptyOrNull())
                original = original.Where(s => s.CarNumber == CarNumber);
            if (ModifiedByUserId.IsNotEmptyOrNull())
                original = original.Where(s => s.ModifiedByUserId == ModifiedByUserId);
            if (Id.IsNotEmptyOrNull())
                original = original.Where(s => s.Id == Id);
            if (Status.IsNotEmptyOrNull())
                original = original.Where(s => s.Status == Status);
            if (Priority.IsNotEmptyOrNull())
                original = original.Where(s => s.Priority == Priority);

            if (SourceId.IsNotEmptyOrNull())
            {
                if (SourceId > 0)
                {
                    original = original.Where(s => s.SourceId == SourceId);
                }
            }
            if (Type.IsNotEmptyOrNull())
            {
                if (Type > 0)
                {
                    original = original.Where(s => s.Type == Type);
                }
            }
            if (MinDate.IsNotEmptyOrNull())
                original = original.Where(s => DbFunctions.TruncateTime(s.UtcDateCreated) >= DbFunctions.TruncateTime(MinDate));
            if (MaxDate.IsNotEmptyOrNull())
                original = original.Where(s => DbFunctions.TruncateTime(s.UtcDateCreated) <= DbFunctions.TruncateTime(MaxDate));

            //Conditional Includes
            if (GetMessageResource.HasValue)
                original = original.Include(r => r.MessageSource);
            if (GetEditions.HasValue)
                original = original.Include(r => r.MessageSource);
            if (GetInvolvedPersons.HasValue)
                original = original.Include(r => r.MessageSource);

            original = original.OrderBy(a => a.Priority).ThenByDescending(a => a.Id); 
            
            if (OrderById.IsNotEmptyOrNull())//Si no es vacio el campo de Ordenar por ID
            {
                switch (OrderById)//Seleccionamos el tipo de Ordenado que Ejecuta
                {
                    case 1:
                        original = original.OrderByDescending(a => a.Id);
                        break;
                    case 2:
                        original = original.OrderBy(a => a.Id);
                        break;
                }
            }
            if (OrderByStatus.IsNotEmptyOrNull())//Si no es vacio el campo de Ordenar por Status
            {
                switch (OrderByStatus)//Seleccionamos el tipo de Ordenado que Ejecuta
                {
                    case 1:
                        original = original.OrderByDescending(a => a.Status).ThenByDescending(a => a.Id);
                        break;
                    case 2:
                        original = original.OrderBy(a => a.Status).ThenByDescending(a => a.Id);
                        break;
                }
            }
            if (OrderByPriority.IsNotEmptyOrNull())//Si no es vacio el campo de Ordenar por Status
            {
                switch (OrderByPriority)//Seleccionamos el tipo de Ordenado que Ejecuta
                {
                    case 1:
                        original = original.OrderBy(a => a.Priority).ThenByDescending(a => a.Id);                        
                        break;
                    case 2:
                        original = original.OrderByDescending(a => a.Priority).ThenByDescending(a => a.Id);
                        break;
                }
            }
                      

            return original;
        }

        public IEnumerable<Report> ExecuteNoEntityRequest(IEnumerable<Report> original)
        {
            if (MinDate.IsNotEmptyOrNull())
                original = original.Where(s => s.UtcDateCreated.Date >= MinDate);
            if (MaxDate.IsNotEmptyOrNull())
                original = original.Where(s => s.UtcDateCreated.Date <= MaxDate);
            return original.OrderByDescending(a => a.Id);
        }

    }
}