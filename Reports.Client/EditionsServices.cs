﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Legendary.Web.Models;
using Reports.Models;
using UserService.Model;

namespace Reports.Client
{
    internal class EditionsServices : RestClient<Edition>
    {
        public EditionsServices(string baseUri)
            : base(new RestServicesWithEncryptedAuthentication(baseUri))
      { }



        public override Edition Update(int id, Edition edition)
        {
            throw new NotImplementedException("Update");
        }

        public override void Delete(int id)
        {
            throw new NotImplementedException("Delete");
        }

    }
}
