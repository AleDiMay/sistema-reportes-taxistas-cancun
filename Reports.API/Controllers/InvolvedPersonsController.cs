﻿using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Legendary.Core.Model;
using Legendary.Core.Utils;
using Reports.Models;
using Reports.Models.Requests;

namespace Reports.API.Controllers
{
    public class InvolvedPersonsController : BaseController
    {
        [UserService.EncryptedApiAuthorization(
            (int) SecurityService.Model.SystemPermissions.ReportsPermissions.ViewInvolvedPerson)]
        public IQueryable<InvolvedPerson> GetInvolvedPersons()
        {
            return db.InvolvedPersons;
        }


        [UserService.EncryptedApiAuthorization(
            (int) SecurityService.Model.SystemPermissions.ReportsPermissions.ViewInvolvedPerson)]
        [ResponseType(typeof (Paginable<InvolvedPerson>))]
        public async Task<IHttpActionResult> GetInvolvedPerson(string request)
        {
            var finalRequest = request.UrlDecode<InvolvedPersonRequest>();
            var types = db.InvolvedPersons;
            var results = await finalRequest.ExecuteRequestAsync(types);
            var paginated = results.Paginate(finalRequest);
            return Ok(paginated);
        }


        [UserService.EncryptedApiAuthorization(
            (int) SecurityService.Model.SystemPermissions.ReportsPermissions.ViewInvolvedPerson)]
        [ResponseType(typeof (InvolvedPerson))]
        public async Task<IHttpActionResult> GetInvolvedPerson(int id)
        {
            InvolvedPerson involvedPerson = await db.InvolvedPersons.FindAsync(id);
            if (involvedPerson == null)
            {
                return NotFound();
            }
            await db.Entry(involvedPerson).Reference(t => t.InvolvedType).LoadAsync();
            return Ok(involvedPerson);
        }


        [UserService.EncryptedApiAuthorization(
            (int) SecurityService.Model.SystemPermissions.ReportsPermissions.EditInvolvedPerson)]
        [ResponseType(typeof (void))]
        public async Task<IHttpActionResult> PutInvolvedPerson(int id, InvolvedPerson involvedPerson)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != involvedPerson.Id)
            {
                return BadRequest();
            }

            db.Entry(involvedPerson).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!InvolvedPersonExists(id))
                {
                    return NotFound();
                }
                throw;
            }

            return StatusCode(HttpStatusCode.NoContent);
        }


        [UserService.EncryptedApiAuthorization(
            (int) SecurityService.Model.SystemPermissions.ReportsPermissions.CreateInvolvedPerson)]
        [ResponseType(typeof (InvolvedPerson))]
        public async Task<IHttpActionResult> PostInvolvedPerson(InvolvedPerson involvedPerson)
        {
            // "Central Standard Time (Mexico)"
            var todayDate = new DateTime();
            todayDate = TimeZoneInfo.ConvertTimeToUtc(DateTime.Now, TimeZoneInfo.Local);
            involvedPerson.UtcDateCreated = todayDate;
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.InvolvedPersons.Add(involvedPerson);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new {id = involvedPerson.Id}, involvedPerson);
        }


        [UserService.EncryptedApiAuthorization(
            (int) SecurityService.Model.SystemPermissions.ReportsPermissions.DeleteInvolvedPerson)]
        [ResponseType(typeof (InvolvedPerson))]
        public async Task<IHttpActionResult> DeleteInvolvedPerson(int id)
        {
            InvolvedPerson involvedPerson = await db.InvolvedPersons.FindAsync(id);
            if (involvedPerson == null)
            {
                return NotFound();
            }

            db.InvolvedPersons.Remove(involvedPerson);
            await db.SaveChangesAsync();

            return Ok(involvedPerson);
        }

        [ResponseType(typeof (InvolvedPerson))]
        public IHttpActionResult Options()
        {
            return Ok(new InvolvedPerson());
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }


        private bool InvolvedPersonExists(int id)
        {
            return db.InvolvedPersons.Count(e => e.Id == id) > 0;
        }
    }
}