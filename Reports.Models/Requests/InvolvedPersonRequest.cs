﻿using System.Linq;
using Legendary.Core.Model.Request;
using Legendary.Core.Utils;

namespace Reports.Models.Requests
{
    public class InvolvedPersonRequest : PaginatedRequest<InvolvedPerson>
    {
        public int ReportId { get; set; }

        public int CreatedByUserId { get; set; }

        public int InvolvedTypeId { get; set; }


        public override IQueryable<InvolvedPerson> ExecuteRequest(IQueryable<InvolvedPerson> original)
        {
            if (ReportId.IsNotEmptyOrNull() && ReportId > 0)
                original = original.Where(c => c.ReportId == ReportId);
            if (CreatedByUserId.IsNotEmptyOrNull() && CreatedByUserId > 0)
                original = original.Where(c => c.CreatedByUserId == CreatedByUserId);
            if (InvolvedTypeId.IsNotEmptyOrNull() && InvolvedTypeId > 0)
                original = original.Where(c => c.InvolvedTypeId == InvolvedTypeId);
            return original;
        }
    }
}