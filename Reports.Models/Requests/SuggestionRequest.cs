﻿using System.Linq;
using Legendary.Core.Model.Request;
using Legendary.Core.Utils;

namespace Reports.Models.Requests
{
    public class SuggestionRequest : PaginatedRequest<Suggestion>
    {
        public int UserId { get; set; }

        public int Type { get; set; }

        public int Status { get; set; }

        public int SourceId { get; set; }

        public override IQueryable<Suggestion> ExecuteRequest(IQueryable<Suggestion> original)
        {
            if (UserId.IsNotEmptyOrNull() && UserId > 0)
                original = original.Where(c => c.UserId == UserId);
            if (Type.IsNotEmptyOrNull() && Type > 0)
                original = original.Where(c => c.Type == Type);
            if (Status.IsNotEmptyOrNull() && Status > 0)
                original = original.Where(c => c.Status == Status);
            if (SourceId.IsNotEmptyOrNull() && SourceId > 0)
                original = original.Where(c => c.SourceId == SourceId);
            return original;
        }
    }
}