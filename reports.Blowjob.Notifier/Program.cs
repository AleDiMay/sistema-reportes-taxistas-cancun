﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Azure.WebJobs;

namespace reports.Blowjob.Notifier
{
    // To learn more about Microsoft Azure WebJobs SDK, please see http://go.microsoft.com/fwlink/?LinkID=320976
    public class Program
    {
        //Variables del Sistema
        private System.Data.SqlClient.SqlConnection connSqlServer;
        private System.Data.SqlClient.SqlCommand cmdSqlServer;
        private System.Data.SqlClient.SqlCommand cmdInsertSqlServer;
        private System.Data.SqlClient.SqlDataReader readerSqlServer;
        private System.Data.SqlClient.SqlDataReader updateIntoDb;
        private String querySqlServer;
        private String updateSqlServer;
        
        static void Main()
        {
            var host = new JobHost();
            // The following code ensures that the WebJob will be running continuously
            host.Call(typeof(Program).GetMethod("GetServices"));
        }

        [NoAutomaticTriggerAttribute]
        public void GetServices()
        {
            //Toma la conexión del App.Config
            String connStringSqlServer = System.Configuration.ConfigurationManager.ConnectionStrings["Tribalance"].ToString();
            //Pasamos los datos de la conexión al conector
            connSqlServer = new System.Data.SqlClient.SqlConnection(connStringSqlServer);
            //Abrimos las conexiones
            connSqlServer.Open();
            DateTime hoy = DateTime.Now;
            //Definimos nuestras consultas
                //Los parametros de la busqueda son:
                    //Seleccionar todo desde dbo.reports
                    //En aquellos registros que:
                    //La fecha de vencimiento sea menor o igual que la fecha de hoy mas 5 dias por ejemplo 10/12/2015 + 5 = 15/12/2015
                    //Y la fecha de vencimiento sea mayor o igual a el dia de hoy
                        //Esto nos arroja los resultados que caduquen en los proximos 5 dias.
                    //El status sea diferente a 3, 5 o nulo esto para evitar ejecutar la acción en registros Cerrados o ya Vencidos.
            querySqlServer = "Select * from dbo.Reports where" + " CONVERT(VARCHAR(8),UtcDateExpiration,112) <= " + hoy.Date.AddDays(5).ToString("yyyyMMdd") + " AND CONVERT(VARCHAR(8),UtcDateExpiration,112) >= " + hoy.Date.ToString("yyyyMMdd") + " AND (Status = 1 OR Status = 2 OR Status = 4 OR Status is null OR Status = 0)";
            //[Unicamente para DEBUG] Imprimimos consulta con los valores ya concatenados.
            Console.WriteLine(querySqlServer);            
            //Construimos las consultas
            cmdSqlServer = new System.Data.SqlClient.SqlCommand(querySqlServer, connSqlServer);
            //Ejecutamos las consultas
            readerSqlServer = cmdSqlServer.ExecuteReader();
            //Verificamos si el Query genero algun resultado
            if (readerSqlServer.HasRows)
            {
                //En caso de haber mas de 0 resultados ejecutamos un "While" lo cual hace un loop por cada resultado.
                while (readerSqlServer.HasRows && readerSqlServer.Read())
                {
                    //Obtenemos la fecha de vencimiento del reporte actual.
                    DateTime ExpiredDate = readerSqlServer.GetDateTime(readerSqlServer.GetOrdinal("UtcDateExpiration"));
                    Console.WriteLine(ExpiredDate.ToString("yyyyMMdd"));
                    //Obtenemos el ID del reporte actual.
                    var idExpired = readerSqlServer.GetInt32(readerSqlServer.GetOrdinal("Id"));
                    //Primero comprobamos si el reporte actual vence el dia de hoy
                    if (ExpiredDate.ToString("yyyyMMdd") == hoy.ToString("yyyyMMdd"))
                    {
                        //En caso de vencer el dia de *hoy*
                        //Generamos un Query que actualiza la prioridad a 0 (Urgente) y el Status a 5 (vencido)
                        updateSqlServer = "Update dbo.reports set Priority = 0, Status = 5 where Id = ";
                        //Concatenamos el Query con el Id del reporte actual, esto da la cadena final del Query
                        updateSqlServer = updateSqlServer + idExpired;
                        //Ejecutamos el Query
                        cmdInsertSqlServer = new System.Data.SqlClient.SqlCommand(updateSqlServer, connSqlServer);
                        //[Unicamente para DEBUG] Imprimimos el resultado final del Query
                        Console.WriteLine(updateSqlServer);
                        updateIntoDb = cmdInsertSqlServer.ExecuteReader();
                        //Leemos la nueva prioridad
                        var Priority = readerSqlServer.GetInt32(readerSqlServer.GetOrdinal("Priority"));
                        //Y el nuevo Status de el reporte actual una vez realizada la actualización
                        var Status = readerSqlServer.GetInt32(readerSqlServer.GetOrdinal("Status"));
                        //[Unicamente para DEBUG] Se imprime la nueva prioridad y el nuevo status como comprobación
                        Console.WriteLine("Se actualizo estatus a " + Status + "Debido a que el reporte Finalizo Hoy");
                    }
                    else
                    {
                        //En caso de no vencer el dia de hoy vence en los proximos 4 días, por lo tanto...
                        //Generamos un Query que actualiza la prioridad a 0 (Urgente) y el Status a 4 (Por vencer)
                        updateSqlServer = "Update dbo.reports set Priority = 0, Status = 4 where Id = ";
                        //Concatenamos el Query con el Id del reporte actual, esto da la cadena final del Query
                        updateSqlServer = updateSqlServer + idExpired;
                        //Ejecutamos el Query
                        cmdInsertSqlServer = new System.Data.SqlClient.SqlCommand(updateSqlServer, connSqlServer);
                        //[Unicamente para DEBUG] Imprimimos el resultado final del Query
                        Console.WriteLine(updateSqlServer);
                        updateIntoDb = cmdInsertSqlServer.ExecuteReader();
                        //Leemos la nueva prioridad
                        var Priority = readerSqlServer.GetInt32(readerSqlServer.GetOrdinal("Priority"));
                        //Y el nuevo Status de el reporte actual una vez realizada la actualización
                        var Status = readerSqlServer.GetInt32(readerSqlServer.GetOrdinal("Status"));
                        //[Unicamente para DEBUG] Se imprime la nueva prioridad y el nuevo status como comprobación
                        Console.WriteLine("Se actualizo estatus a " + Status + " Debido a que el reporte Finaliza el dia " + ExpiredDate.ToString("dd MMMM yyyy"));   
                    }                    
                //Cerramos el "While" por lo tanto ya no se muestran mas elementos en loop                    
                }
                //Cerramos la conexión a base de datos.
                readerSqlServer.Close();
            }
            else
            {
                //En caso de no haber nada a mostar imprimimos:
                Console.Out.Write("No hay nada que mostrar!");
            }
            //[Unicamente para DEBUG] Esperamos una respuesta de la consola.
            Console.Read();
            
        }

        public object UtcDateExpiration { get; set; }
    }
}
