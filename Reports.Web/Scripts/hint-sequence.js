var enjoyhint_instance = new EnjoyHint({});

var enjoyhint_script_steps = [
  {
  	'next .navbar-brand' : 'Bienvenido a la nueva versión de Vigía Ciudadano.<br>A traves de este breve tutorial te enseñaremos algunas nuevas características.',
  	'nextButton' : {className: "myNext", text: "Continuar"},
  	'skipButton' : {className: "mySkip", text: "Saltar"}
  },
  {
  	'click .open-filter' : 'El sistema de <b>Filtrado</b> es útil para encontrar un registro o un grupo de registros en específico.<br>Prueba haciendo <i>Click</i> sobre él ahora.',
  	'showSkip' : false
  },
  {
  	'next .aplicate-filter' : 'Puedes hacer <i>Click</i> en este boton para buscar.',
  	'nextButton' : {className: "myNext", text: "Continuar"},
  	'showSkip' : false
  },
  {
  	'click .close-filter' : 'O bien usar el boton de cancelar para salir del filtrado<br>Pruba haciendo <i>Click sobre él ahora.',
  	'nextButton' : {className: "myNext", text: "Continuar"},
  	'showSkip' : false
  },
  {
  	'next .reportunit' : 'Cada fila representa un reporte, los marcados en <b>Negritas</b> son reportes aún sin leer.',
  	'nextButton' : {className: "myNext", text: "Continuar"},
  	'showSkip' : false
  },
  {
  	'next .createnew' : 'Usa este boton para crear un nuevo reporte.',
  	'nextButton' : {className: "myNext", text: "Continuar"},
  	'showSkip' : false
  },
  {
  	'next .navbar-brand' : 'Esta nueva presentación tiene el fin de hacer mas faciles y amigables los procesos de esta aplicación.<br>Si tienes alguna duda sobre su uso o la ubicación de ciertos datos, comunicate al departamenteo de desarrollo en la extensión <b>2603</b>',
  	'nextButton' : {className: "myNext", text: "Terminar"},
  	'showSkip' : false
  }

];

enjoyhint_instance.set(enjoyhint_script_steps);