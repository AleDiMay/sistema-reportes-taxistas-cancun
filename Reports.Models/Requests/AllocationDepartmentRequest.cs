﻿using System.Linq;
using Legendary.Core.Model.Request;
using Legendary.Core.Utils;

namespace Reports.Models.Requests
{
    public class AllocationDepartmentRequest : PaginatedRequest<AllocationDepartment>
    {
        public int? Id { get; set; }

        public string Name { get; set; }

        public int? AllowedDepartment { get; set; }

        public override IQueryable<AllocationDepartment> ExecuteRequest(IQueryable<AllocationDepartment> original)
        {
            if (Id.IsNotEmptyOrNull() && Id > 0)
                original = original.Where(c => c.Id == Id);

            if (Name.IsNotEmptyOrNull())
                original = original.Where(c => c.Name == Name);

            if (AllowedDepartment.IsNotEmptyOrNull() && AllowedDepartment > 0)
                original =
                    original.Where(
                        c =>
                            !string.IsNullOrEmpty(c.AvailableAllocationDepartments) &&
                            c.AvailableAllocationDepartments.Contains(AllowedDepartment.ToString()));

            return original;
        }
    }
}