﻿using System.Web.Mvc;
using Legendary.Core.Utils;
using UserService.Model;

namespace Reports.API.Controllers
{
    public class AccountController : Controller
    {
        // GET: Account
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Login(string absoluteUri)
        {
            return RedirectResult(new LoginRequest(), "Account", absoluteUri, false);
        }

        [ValidateAntiForgeryToken, HttpPost]
        public ActionResult Login(LoginRequest loginRequest, string absoluteUri)
        {
            if (ModelState.IsValid)
            {
                return RedirectResult(loginRequest, "Account", absoluteUri, true);
            }
            return View(loginRequest);
        }


        public ActionResult RedirectResult(LoginRequest loginRequest, string viewName, string absoluteUri, bool tryLogin)
        {
            ViewBag.absolutUri = absoluteUri;
            var isLogged = UserService.LoginManager.IsLoggedIn();
            if (!isLogged)
            {
                if (tryLogin)
                {
                    loginRequest.System = SecurityService.Model.Enums.Systems.Reports;
                    var result = UserService.LoginManager.Login(loginRequest);
                    if (result == Enums.LoginResult.Succesful)
                    {
                        if (absoluteUri.IsNotEmptyOrNull())
                            return Redirect(absoluteUri);
                        return RedirectToAction("Index", "Home");
                    }
                    ViewBag.errorMessage = "Email o password inválidos";
                }
                return View(viewName, loginRequest);
            }
            if (absoluteUri.IsNotEmptyOrNull())
                return Redirect(absoluteUri);
            return RedirectToAction("Index", "Home");
        }

        public ActionResult Logout()
        {
            UserService.LoginManager.Logout();
            return RedirectToAction("Login", "Account");
        }
    }
}