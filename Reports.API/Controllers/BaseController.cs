﻿using System.Web.Http;
using Reports.Models;
using UserService;

namespace Reports.API.Controllers
{
    [EncryptedApiAuthentication]
    public class BaseController : ApiController
    {
        protected readonly ReportsEntities db;

        public BaseController()
        {
            db = new ReportsEntities();
            db.Configuration.LazyLoadingEnabled = false;
            db.Configuration.ProxyCreationEnabled = false;
        }
    }
}