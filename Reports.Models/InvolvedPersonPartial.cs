﻿using System.ComponentModel.DataAnnotations;

namespace Reports.Models
{
    [MetadataType(typeof (InvolvedPersonMd))]
    public partial class InvolvedPerson
    {
    }

    public class InvolvedPersonMd
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "El nombre de la persona involucrada es necesario")]
        public string Name { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Es necesario ingresar un número de telefono")]
        public string Phone { get; set; }
    }
}