﻿using System.Web.Mvc;
using CommonService;
using Reports.Client;
using Reports.Models;

namespace Reports.Application.Controllers
{
    public class ReportTypesController : Controller
    {
        private readonly GeneralReportServices _generalReportServices;

        public ReportTypesController()
        {
            _generalReportServices = new GeneralReportServices(SiteParameter.Get("reports.api").Get<string>());
        }

        // GET: ReportTypes
        public ActionResult Index()
        {
            var reportTypes = _generalReportServices.GetReportTypes();
            return View(reportTypes);
        }

        public ActionResult Create()
        {
            return View();
        }


        [HttpPost]
        public ActionResult Create(ReportType reportType)
        {
            if (!ModelState.IsValid)
                return View(reportType);


            _generalReportServices.CreateReportType(reportType);
            return RedirectToAction("Index", "ReportTypes");
        }

    }
}