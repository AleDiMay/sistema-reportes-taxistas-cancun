﻿using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Reports.Models;

namespace Reports.API.Controllers
{
    public class MessageSourcesController : BaseController
    {
        [UserService.EncryptedApiAuthorization(
            (int) SecurityService.Model.SystemPermissions.ReportsPermissions.ViewMessageSource)]
        public IQueryable<MessageSource> GetMessageSources()
        {
            return db.MessageSources;
        }


        // GET: api/MessageSources/5
        [UserService.EncryptedApiAuthorization(
            (int) SecurityService.Model.SystemPermissions.ReportsPermissions.ViewMessageSource)]
        [ResponseType(typeof (MessageSource))]
        public async Task<IHttpActionResult> GetMessageSource(int id)
        {
            MessageSource reportType = await db.MessageSources.FindAsync(id);
            if (reportType == null)
            {
                return NotFound();
            }

            return Ok(reportType);
        }


        // PUT: api/Reports/5
        [UserService.EncryptedApiAuthorization(
            (int) SecurityService.Model.SystemPermissions.ReportsPermissions.EditMessageSource)]
        [ResponseType(typeof (void))]
        public async Task<IHttpActionResult> PutMessageSource(int id, MessageSource messageSource)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != messageSource.Id)
            {
                return BadRequest();
            }

            db.Entry(messageSource).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MessageSourceExists(id))
                {
                    return NotFound();
                }
                throw;
            }

            return StatusCode(HttpStatusCode.NoContent);
        }


        // POST: api/Reports
        [UserService.EncryptedApiAuthorization(
            (int) SecurityService.Model.SystemPermissions.ReportsPermissions.CreateMessageSource)]
        [ResponseType(typeof (MessageSource))]
        public async Task<IHttpActionResult> PostMessageSource(MessageSource messageSource)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.MessageSources.Add(messageSource);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new {id = messageSource.Id}, messageSource);
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool MessageSourceExists(int id)
        {
            return db.MessageSources.Count(e => e.Id == id) > 0;
        }
    }
}