﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using CommonService;
using CommonService.Filters;
using Legendary.Core.Utils;
using Reports.Application.Helpers;
using Reports.Application.Models;
using Reports.Client;
using Reports.Models;
using Reports.Models.Requests;
using SecurityService.Model;
using Padron.Client;
using Padron.Model.Requests;

namespace Reports.Application.Controllers
{
    public class ReportsController : BaseLoggedController
    {

        private readonly GeneralReportServices _generalReportServices;
        private DepartmentService _directorioService;
        private PadronFacade _empleadosService;

        public ReportsController()
        {
            //_empleadosService = new PadronFacade(SiteParameter.Get("directorio.api").Get<string>());
            _generalReportServices = new GeneralReportServices(SiteParameter.Get("reports.api").Get<string>());
            //_directorioService = new DepartmentService(SiteParameter.Get("directorio.api").Get<string>());
        }

        [UserValidatorPermission((int)SystemPermissions.ReportsPermissions.Login)]
        public ActionResult Index(PaginatedReports paginatedReports = null)
        {
            
            if (paginatedReports.Request.IsEmptyOrNull())
            {
                var request = new ReportsRequest();
             
                request.Page = paginatedReports.SelectedPage > 0 ? (int)paginatedReports.SelectedPage : 1;
                request.PageSize = 10;
                paginatedReports.Request = request;
                
                
            }
            else
            {
                paginatedReports.Request.Page = paginatedReports.SelectedPage > 0 ? (int)paginatedReports.SelectedPage : 1;
                paginatedReports.Request.PageSize = 10;
            }
            paginatedReports.SelectedPage = paginatedReports.SelectedPage > 0 ? (int)paginatedReports.SelectedPage : 1;
            var paginatedItemList = _generalReportServices.GetReport(paginatedReports.Request);
            paginatedReports.TotalPages = (paginatedItemList.TotalCount / 10) + 1;

            paginatedReports.Reports = paginatedItemList.AsEnumerable();
            
            return View(paginatedReports);
        }
        
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [UserValidatorPermission((int)SystemPermissions.ReportsPermissions.CreateReport)]
        public ActionResult Create(Report report)
        {
            // "Central Standard Time (Mexico)"
            var todayDate = new DateTime();
            todayDate = TimeZoneInfo.ConvertTimeToUtc(DateTime.Now, TimeZoneInfo.Local);
            var expirationDate = todayDate.AddDays(15);
            report.UtcDateExpiration = expirationDate;
            report.UtcDateCreated = todayDate;
            report.Status = 0;
            report.UtcDateModified = report.UtcDateCreated;
            report.UserId = UserService.LoginManager.GetLoggedUser().userId;
            report.ModifiedByUserId = report.UserId;

            if (!ModelState.IsValid)
                return View(report);


            var newReport = _generalReportServices.CreateReport(report);

            return RedirectToAction("Details", "Reports", new { reportId = newReport.Id });
        }
        [UserValidatorPermission((int)SystemPermissions.ReportsPermissions.ViewReport)]
        public ActionResult Details(int reportId)
        {
            //var users = _empleadosService.UserService.Get(new UserRequest { });
            //var user = _service.UserService.Get(new UserRequest { Ids = new List<int> { users.userId }, GetEmail = true, GetAddress = true, GetPhone = true, GetHealthInformation = true }).Items.First();
            //user = _empleadosService.UserService.Get(new UserRequest { Ids = new List<int> { users.userId }, GetOcupation = true, GetEmail = true }).Items.First();
            //ViewBag.empleadosList = new SelectList(users.AsEnumerable(), "partnerReference", "Name");
            //ViewBag.empleadosLists = new SelectList(users.ConvertToList(), "partnerReference", "Name");
            //var departmentsList = _directorioService.Get();
            //ViewBag.listadepartamento = new SelectList(departmentsList, "Id", "Name");
            var rport = _generalReportServices.GetReport(reportId);
            var isLoggedUserSuperUser = SecurityService.Manager.IsSuperUser(SecurityService.Manager.GetUserId());
            var isAdministrator = SecurityService.Manager.HasPermissions(UserService.LoginManager.GetLoggedUser().userId, (int)SecurityService.Model.SystemPermissions.ReportsPermissions.IsAdministrator);
            if (!isLoggedUserSuperUser && !isAdministrator)
            {
                if (rport.Status == 0 || rport.Type.IsEmptyOrNull())
                {
                    rport.Status = 1;
                    rport.UtcDateModified = TimeZoneInfo.ConvertTimeToUtc(DateTime.Now, TimeZoneInfo.Local).Date;
                    rport.ModifiedByUserId = UserService.LoginManager.GetLoggedUser().userId;
                    if (rport.Type == null || rport.Type == 0)
                    {
                        rport.Type = SiteOptions.GetReportTypeNone();
                    }
                    _generalReportServices.UpdateReport(rport);
                }
            }
            else
            {
                if (rport.Type == null || rport.Type == 0)
                {
                    rport.Type = SiteOptions.GetReportTypeNone();
                    _generalReportServices.UpdateReport(rport);
                }
            }
            return View(SiteOptions.CreateViewReportModel(rport, _generalReportServices.GetReports()));
        }
        //Modulo de Impresión
        public ActionResult printReport(int reportId)
        {
            var rport = _generalReportServices.GetReport(reportId);
            var isLoggedUserSuperUser = SecurityService.Manager.IsSuperUser(SecurityService.Manager.GetUserId());
            var isAdministrator = SecurityService.Manager.HasPermissions(UserService.LoginManager.GetLoggedUser().userId, (int)SecurityService.Model.SystemPermissions.ReportsPermissions.IsAdministrator);
            if (!isLoggedUserSuperUser && !isAdministrator)
            {
                if (rport.Status == 0 || rport.Type.IsEmptyOrNull())
                {
                    rport.Status = 1;
                    rport.UtcDateModified = TimeZoneInfo.ConvertTimeToUtc(DateTime.Now, TimeZoneInfo.Local).Date;
                    rport.ModifiedByUserId = UserService.LoginManager.GetLoggedUser().userId;
                    if (rport.Type == null || rport.Type == 0)
                    {
                        rport.Type = SiteOptions.GetReportTypeNone();
                    }
                    _generalReportServices.UpdateReport(rport);
                }
            }
            else
            {
                if (rport.Type == null || rport.Type == 0)
                {
                    rport.Type = SiteOptions.GetReportTypeNone();
                    _generalReportServices.UpdateReport(rport);
                }
            }
            return View("~/Views/Reports/printReport.cshtml", SiteOptions.CreateViewReportModel(rport, _generalReportServices.GetReports()));
        }
        //Fin Modulo de Impresión
        public ActionResult ReportStatistics(ReportsStatistics reportsStatistics = null)
        {
            if (!reportsStatistics.ReportsRequest.IsNotEmptyOrNull())
                reportsStatistics.ReportsRequest = new ReportsRequest();
            var originalList = _generalReportServices.GetReports();
            var totalPaginatedReports = reportsStatistics.ReportsRequest.ExecuteNoEntityRequest(originalList);
            if (totalPaginatedReports.IsNotEmptyOrNull() && totalPaginatedReports.IsNotEmptyOrNull())
            {
                var enumerable = totalPaginatedReports as IList<Report> ?? totalPaginatedReports.ToList();
                reportsStatistics.TotalReports = enumerable.Count();
                reportsStatistics.ReportSourcesStatistics =
                    SiteOptions.GetReportSourceStatistics(SiteOptions.GetMessageSourceList(), enumerable);
                reportsStatistics.ReportTypesStatistics =
                    SiteOptions.GetReportTypesStatistics(SiteOptions.GetReportTypesList(), enumerable);
            }
            return View(reportsStatistics);
        }

        [UserValidatorPermission((int)SystemPermissions.ReportsPermissions.ReopenReport)]
        public ActionResult ReOpen(int reportId)
        {
            var rport = _generalReportServices.GetReport(reportId);
            if (rport.IsNotEmptyOrNull())
            {
                if (rport.Status == (int)Reports.Models.Enums.ReportsStatus.Closed)
                {
                    rport.Status = (int)Reports.Models.Enums.ReportsStatus.InFollowing;
                    _generalReportServices.UpdateReport(rport);

                    var todayDate = new DateTime();
                    todayDate = TimeZoneInfo.ConvertTimeToUtc(DateTime.Now, TimeZoneInfo.Local);
                    var closeEdition = new Reports.Models.Edition();
                    closeEdition.EditedById = UserService.LoginManager.GetLoggedUser().userId;
                    closeEdition.ReportId = rport.Id;
                    closeEdition.UTCDateEdited = todayDate;
                    closeEdition.Content = "Reabierto por: " + SiteOptions.GetUserShortName(UserService.LoginManager.GetLoggedUser().userId);
                    _generalReportServices.CreateEdition(closeEdition);
                }
                return RedirectToAction("Details", "Reports", new { reportId = rport.Id });
            }
            return RedirectToAction("Index", "Reports");
        }

    }
}