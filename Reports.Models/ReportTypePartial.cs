﻿using System.ComponentModel.DataAnnotations;

namespace Reports.Models
{
    [MetadataType(typeof (ReportTypeMd))]
    public partial class ReportType
    {
    }

    public class ReportTypeMd
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "Un nombre para el tipo de reporte es necesario")]
        public string Name { get; set; }
    }
}