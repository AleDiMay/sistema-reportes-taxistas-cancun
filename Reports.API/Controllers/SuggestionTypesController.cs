﻿using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Reports.Models;

namespace Reports.API.Controllers
{
    public class SuggestionTypesController : BaseController
    {
        //private ReportsEntities db = new ReportsEntities();

        public IQueryable<SuggestionType> GetSuggestionTypes()
        {
            return db.SuggestionTypes;
        }


        // GET: api/Reports/5
        [ResponseType(typeof(SuggestionType))]
        [UserService.EncryptedApiAuthorization((int)SecurityService.Model.SystemPermissions.ReportsPermissions.ViewSuggestion)]
        public async Task<IHttpActionResult> GetSuggestionType(int id)
        {
            SuggestionType suggestionType = await db.SuggestionTypes.FindAsync(id);
            if (suggestionType == null)
            {
                return NotFound();
            }

            return Ok(suggestionType);
        }


        // PUT: api/Reports/5
        [ResponseType(typeof(void))]
        [UserService.EncryptedApiAuthorization((int)SecurityService.Model.SystemPermissions.ReportsPermissions.EditSuggestion)]
        public async Task<IHttpActionResult> PutSuggestionType(int id, SuggestionType suggestionType)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != suggestionType.Id)
            {
                return BadRequest();
            }

            db.Entry(suggestionType).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SuggestionTypeExists(id))
                {
                    return NotFound();
                }
                throw;
            }

            return StatusCode(HttpStatusCode.NoContent);
        }


        // POST: api/Reports
        [ResponseType(typeof(SuggestionType))]
        [UserService.EncryptedApiAuthorization((int)SecurityService.Model.SystemPermissions.ReportsPermissions.CreateSuggestion)]
        public async Task<IHttpActionResult> PostSuggestionType(SuggestionType suggestionType)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.SuggestionTypes.Add(suggestionType);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new {id = suggestionType.Id}, suggestionType);
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool SuggestionTypeExists(int id)
        {
            return db.SuggestionTypes.Count(e => e.Id == id) > 0;
        }
    }
}
