﻿using System.Linq;
using Legendary.Core.Model.Request;
using Legendary.Core.Utils;

namespace Reports.Models.Requests
{
    public class EditionRequest : PaginatedRequest<Edition>
    {
        public int EditedById { get; set; }

        public int ReportId { get; set; }

        public override IQueryable<Edition> ExecuteRequest(IQueryable<Edition> original)
        {
            if (EditedById.IsNotEmptyOrNull() && EditedById > 0)
                original = original.Where(c => c.EditedById == EditedById);
            if (ReportId.IsNotEmptyOrNull() && ReportId > 0)
                original = original.Where(c => c.ReportId == ReportId);
            return original;
        }
    }
}