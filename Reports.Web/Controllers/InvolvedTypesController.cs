﻿using System.Web.Mvc;
using CommonService;
using Reports.Client;
using Reports.Models;

namespace Reports.Application.Controllers
{
    public class InvolvedTypesController : Controller
    {
        private readonly GeneralReportServices _generalReportServices;

        public InvolvedTypesController()
        {
            _generalReportServices = new GeneralReportServices(SiteParameter.Get("reports.api").Get<string>());
        }

        // GET: InvolvedTypes
        public ActionResult Index()
        {
            var involvedTypesList = _generalReportServices.GetInvolvedTypes();
            return View(involvedTypesList);
        }

        public ActionResult Create()
        {
           return View();            
        }

        [HttpPost]
        public ActionResult Create(InvolvedType involvedType)
        {
            if (!ModelState.IsValid)
                return View(involvedType);

            _generalReportServices.CreateInvolvedType(involvedType);
            return RedirectToAction("Index");
        }

    }
}