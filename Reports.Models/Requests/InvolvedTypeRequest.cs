﻿using System.Linq;
using Legendary.Core.Model.Request;
using Legendary.Core.Utils;

namespace Reports.Models.Requests
{
    public class InvolvedTypeRequest : PaginatedRequest<InvolvedType>
    {
        public int Id { get; set; }

        public override IQueryable<InvolvedType> ExecuteRequest(IQueryable<InvolvedType> original)
        {
            if (Id.IsNotEmptyOrNull() && Id > 0)
                original = original.Where(c => c.Id == Id);
            return original;
        }
    }
}