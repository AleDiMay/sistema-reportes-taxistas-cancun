﻿using System.ComponentModel.DataAnnotations;

namespace Reports.Models
{
    [MetadataType(typeof (InvolvedTypeMd))]
    public partial class InvolvedType
    {
    }

    public class InvolvedTypeMd
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "Un nombre es requerido")]
        public string Name { get; set; }
    }
}