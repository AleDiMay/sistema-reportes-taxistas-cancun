﻿using System;
using System.Linq;
using System.Web.Mvc;
using CommonService;
using Legendary.Core.Utils;
using Reports.Application.Filters;
using Reports.Application.Helpers;
using Reports.Application.Models;
using Reports.Client;
using Reports.Models.Requests;
using SecurityService.Model;

namespace Reports.Application.Controllers
{
    public class HomeController : BaseLoggedController
    {
        //private readonly CasesServices _cService;
        private readonly GeneralReportServices _generalReportServices;

        public HomeController()
        {
            //_cService = new CasesServices(SiteParameter.Get("cases.api").Get<string>(), ConfigurationManager.AppSettings["cases.api.key"]);
            _generalReportServices = new GeneralReportServices(SiteParameter.Get("reports.api").Get<string>());
        }

        [UserValidatorPermission((int)SystemPermissions.ReportsPermissions.Login)]
        public ActionResult Index()
        {
            return RedirectToAction("Index", "Reports");
        }

        public ActionResult ShowReports(PaginatedReports paginatedReports = null)
        {
            if (paginatedReports.Request.IsEmptyOrNull())
            {
                var request = new ReportsRequest();
                request.Page = paginatedReports.SelectedPage > 0 ? (int)paginatedReports.SelectedPage : 1;
                request.PageSize = 20;
                paginatedReports.Request = request;
            }
            else
            {
                new ReportsRequest();
                paginatedReports.Request.Page = paginatedReports.SelectedPage > 0 ? (int)paginatedReports.SelectedPage : 1;
                paginatedReports.Request.PageSize = 20;
                if (paginatedReports.Request.SourceId == 0)
                {
                    paginatedReports.Request.SourceId = null;
                }
            }
            paginatedReports.SelectedPage = paginatedReports.SelectedPage > 0 ? (int)paginatedReports.SelectedPage : 1;
            var paginatedItemList = _generalReportServices.GetReport(paginatedReports.Request);
            paginatedReports.TotalPages = paginatedItemList.TotalCount;
            paginatedReports.Reports = paginatedItemList.AsEnumerable();
            return View(paginatedReports);
        }

        public ActionResult ShowReportDescription(int reportId)
        {
            var rport = _generalReportServices.GetReport(reportId);
            var isLoggedUserSuperUser = SecurityService.Manager.IsSuperUser(SecurityService.Manager.GetUserId());
            var isAdministrator = SecurityService.Manager.HasPermissions(UserService.LoginManager.GetLoggedUser().userId, (int)SecurityService.Model.SystemPermissions.ReportsPermissions.IsAdministrator);
            if (!isLoggedUserSuperUser && !isAdministrator)
            {
                if (rport.Status == 0 || rport.Type.IsEmptyOrNull())
                {
                    rport.Status = 1;
                    rport.UtcDateModified = TimeZoneInfo.ConvertTimeToUtc(DateTime.Now, TimeZoneInfo.Local);
                    rport.ModifiedByUserId = UserService.LoginManager.GetLoggedUser().userId;
                    if (rport.Type == null || rport.Type == 0)
                    {
                        rport.Type = SiteOptions.GetReportTypeNone();
                    }
                    _generalReportServices.UpdateReport(rport);
                }                
            }
            else
            {
                if (rport.Type == null || rport.Type == 0)
                {
                    rport.Type = SiteOptions.GetReportTypeNone();
                    _generalReportServices.UpdateReport(rport);
                }   
            }
            return View("~/Views/Home/Partials/Report.cshtml", SiteOptions.CreateViewReportModel(rport, _generalReportServices.GetReports()));
        }

        [UserValidatorPermission((int)SystemPermissions.ReportsPermissions.ViewSuggestions)]
        public ActionResult ShowSuggestions()
        {
            var suggestionList = _generalReportServices.GetSuggestions();
            suggestionList = suggestionList.OrderByDescending(a => a.Date);
            ViewData["listOfSuggestions"] = suggestionList;
            return View();
        }

        public ActionResult ShowSuggestionDescription(int suggestionId)
        {
            var rport = _generalReportServices.GetSuggestion(suggestionId);
            var isLoggedUserSuperUser = SecurityService.Manager.IsSuperUser(SecurityService.Manager.GetUserId());
            var isAdministrator = SecurityService.Manager.HasPermissions(UserService.LoginManager.GetLoggedUser().userId, (int)SecurityService.Model.SystemPermissions.ReportsPermissions.IsAdministrator);
            if (!isLoggedUserSuperUser && !isAdministrator)
            {
                if (rport.Status == 0 || rport.Type.IsEmptyOrNull())
                {
                    rport.Status = 1;
                    rport.DateModified = TimeZoneInfo.ConvertTimeToUtc(DateTime.Now, TimeZoneInfo.Local);
                    rport.ModifiedByUserId = UserService.LoginManager.GetLoggedUser().userId;
                    rport.Type = rport.Type ?? 0;
                     _generalReportServices.UpdateSuggestion(rport);
                }                
            }
            return View("~/Views/Home/Partials/Suggestion.cshtml", rport);
        }



        public ActionResult SendResponseEmail(string emailAddressToSend,int reportId)
        {
            ViewData["emailAddressToSend"] = emailAddressToSend;
            ViewData["reportId"] = reportId;
            return View("~/Views/Home/Partials/EmailForm.cshtml");
        }


        public ActionResult CreateCase(int rId)
        {
            TimeZoneInfo mxZone = TimeZoneInfo.FindSystemTimeZoneById("Central Standard Time (Mexico)");      
            var reportFrom = _generalReportServices.GetReport(rId);
            if (reportFrom.IsNotEmptyOrNull() && reportFrom.Status != (int)Reports.Models.Enums.ReportsStatus.InFollowing)
            {
                var newCase = new Cases.Models.Case();
                newCase.UserCreatedId = UserService.LoginManager.GetLoggedUser().userId;
                newCase.Section = (int)Cases.Models.Enums.Sections.SecretaryOfLabor;
                newCase.Status = (int) Cases.Models.Enums.Status.NotOpened;
                newCase.UTCDateCreated = TimeZoneInfo.ConvertTimeToUtc(DateTime.Now, TimeZoneInfo.Local);
                newCase.UTCDueDate = TimeZoneInfo.ConvertTimeToUtc(DateTime.Now.AddDays(3), TimeZoneInfo.Local);
                newCase.Type = (int) Cases.Models.Enums.SecretaryOfLaborCasesType.Incidents;
                newCase.Subject = SiteOptions.GetReportType(reportFrom.Type);
                newCase.RelatedCaseId = reportFrom.Id;
                newCase.RelatedCaseType = (int) Cases.Models.Enums.RelatedCaseType.Report;
                newCase.AssignedToId = 0;

                var newEdit = new Cases.Models.Version();
                newEdit.Status = newCase.Status;
                newEdit.UTCDateModified = newCase.UTCDateCreated;
                newEdit.UserModifiedId = newCase.UserCreatedId;
                newEdit.Content = "Creado desde el reporte padre número: " + reportFrom.Id +Environment.NewLine;
                newCase.Versions.Add(newEdit);
                //_cService.SaveCase(newCase);
                reportFrom.Status = (int) Reports.Models.Enums.ReportsStatus.InFollowing;
                _generalReportServices.UpdateReport(reportFrom);
            }
            return RedirectToAction("ShowReports");
        }


        public ActionResult AddReport()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}