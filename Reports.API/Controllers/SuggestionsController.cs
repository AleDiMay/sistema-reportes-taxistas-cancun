﻿using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;
using Reports.Models;

namespace Reports.API.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class SuggestionsController : BaseController
    {
        // GET: api/Suggestions
        [UserService.EncryptedApiAuthorization(
            (int) SecurityService.Model.SystemPermissions.ReportsPermissions.ViewSuggestion)]
        public IQueryable<Suggestion> GetSuggestions()
        {
            return db.Suggestions;
        }

        // GET: api/Suggestions/5
        [ResponseType(typeof (Suggestion))]
        [UserService.EncryptedApiAuthorization(
            (int) SecurityService.Model.SystemPermissions.ReportsPermissions.ViewSuggestion)]
        public async Task<IHttpActionResult> GetSuggestion(int id)
        {
            Suggestion suggestion = await db.Suggestions.FindAsync(id);
            if (suggestion == null)
            {
                return NotFound();
            }

            return Ok(suggestion);
        }

        // PUT: api/Suggestions/5
        [ResponseType(typeof (void))]
        [UserService.EncryptedApiAuthorization(
            (int) SecurityService.Model.SystemPermissions.ReportsPermissions.EditSuggestion)]
        public async Task<IHttpActionResult> PutSuggestion(int id, Suggestion suggestion)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != suggestion.Id)
            {
                return BadRequest();
            }

            db.Entry(suggestion).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SuggestionExists(id))
                {
                    return NotFound();
                }
                throw;
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Suggestions
        [ResponseType(typeof (Suggestion))]
        [UserService.EncryptedApiAuthorization(
            (int) SecurityService.Model.SystemPermissions.ReportsPermissions.CreateSuggestion)]
        public async Task<IHttpActionResult> PostSuggestion(Suggestion suggestion)
        {
            var newDate = new DateTime();
            newDate = DateTime.Now;
            suggestion.Date = newDate;
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Suggestions.Add(suggestion);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (SuggestionExists(suggestion.Id))
                {
                    return Conflict();
                }
                throw;
            }

            return CreatedAtRoute("DefaultApi", new {id = suggestion.Id}, suggestion);
        }

        // DELETE: api/Suggestions/5
        [ResponseType(typeof (Suggestion))]
        [UserService.EncryptedApiAuthorization(
            (int) SecurityService.Model.SystemPermissions.ReportsPermissions.DeleteSuggestion)]
        public async Task<IHttpActionResult> DeleteSuggestion(int id)
        {
            Suggestion suggestion = await db.Suggestions.FindAsync(id);
            if (suggestion == null)
            {
                return NotFound();
            }

            db.Suggestions.Remove(suggestion);
            await db.SaveChangesAsync();

            return Ok(suggestion);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool SuggestionExists(int id)
        {
            return db.Suggestions.Count(e => e.Id == id) > 0;
        }
    }
}