﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CommonService;
using CommonService.Models;
using Legendary.Core.Utils;
using Legendary.Web.Models;
using Legendary.Cache;
using Reports.Application.Models;
using Reports.Client;
using Reports.Models;
using Padron.Client;
using UserService.Model;
using UserRequest = Padron.Model.Requests.UserRequest;

namespace Reports.Application.Helpers
{
    public static class SiteOptions
    {
        private static readonly PadronFacade Padron = new PadronFacade(SiteParameter.Get("apiUri").Get<string>());
        //Methods for login
        public static void PrefilterUserId(ref int requestUserId)
        {
            var request = GetPrefilteredRequest();
            if (request == null) return;

            //prefiltrar el userid
            if (!request.Ids.Contains(requestUserId))
            {
                requestUserId = 0;
            }
        }

        public static bool HasPrefilteredRequest
        {
            get
            {
                return GetPrefilteredRequest() != null;
            }
        }

        public static void PreFilterRequest(ref UserRequest originalRequest)
        {

            var prefilteredRequest = GetPrefilteredRequest();
            if (prefilteredRequest == null) return;

            prefilteredRequest.Page = originalRequest.Page;
            prefilteredRequest.PageSize = originalRequest.PageSize;
            prefilteredRequest.TotalCount = originalRequest.TotalCount;
            prefilteredRequest.GetEmail = originalRequest.GetEmail;
            prefilteredRequest.GetAddress = originalRequest.GetAddress;
            prefilteredRequest.GetHealthInformation = originalRequest.GetHealthInformation;
            prefilteredRequest.GetPhone = originalRequest.GetPhone;
            prefilteredRequest.Name = originalRequest.Name;
            prefilteredRequest.MaxAge = originalRequest.MaxAge;
            prefilteredRequest.MinAge = originalRequest.MinAge;
            prefilteredRequest.PartnerReference = originalRequest.PartnerReference;
            originalRequest = prefilteredRequest;
        }

        public static user GetCurrentUser()
        {
            return UserService.LoginManager.GetLoggedUser();
        }

        private static UserRequest GetPrefilteredRequest()
        {
            var request = HttpContext.Current.Session["currentUserRequest_" + SiteOptions.GetCurrentUser().userId] as UserRequest;

            if (request != null) return request;

            var currentUser = UserService.LoginManager.GetLoggedUser();
            if (currentUser != null)
            {
                var preRequest = Padron.UserPreassignedRequestService.Get(currentUser.userId);
                if (preRequest.userPreassignedRequestId != 0)
                {
                    request = preRequest.objectRequest.DeserializeFromJSON<UserRequest>();
                }
            }

            HttpContext.Current.Session["currentUserRequest_" + SiteOptions.GetCurrentUser().userId] = request;
            return request;
        }

        public static string GetReportStatusText(int? status)
        {
            var message = string.Empty;
            switch (status)
            {
                case 0:
                    message = "Sin leer";
                    break;
                case 1:
                    message = "En proceso";
                    break;
                case 2:
                    message = "En seguimiento";
                    break;
                case 3:
                    message = "Cerrado";
                    break;
                case 4:
                    message = "Por Expirar";
                    break;
                case 5:
                    message = "Expirado";
                    break;
                default:
                    message = "Sin leer";
                    break;
            }
            return message;
        }

        public static string GetCSSClassByStatus(int? status)
        {
            var message = string.Empty;
            switch (status)
            {
                case 0:
                    message = "newprimary";
                    break;
                case 1:
                    message = "info";
                    break;
                case 2:
                    message = "info";
                    break;
                case 3:
                    message = "success";
                    break;
                case 4:
                    message = "warning";
                    break;
                case 5:
                    message = "danger";
                    break;
                default:
                    message = "newprimary";
                    break;
            }
            return message;
        }
        //Helper para obtener el Valor desde el INT de Prioridad
        //Case 0 = Urgente ... ETC
        public static string GetReportPriorityText(int? Priority)
        {
            var message = string.Empty;
            switch (Priority)
            {
                case 0:
                    message = "Urgente";
                    break;
                case 1:
                    message = "Alta";
                    break;
                case 2:
                    message = "Normal";
                    break;
                case 3:
                    message = "Baja";
                    break;
                default:
                    message = "Normal";
                    break;
            }
            return message;
        }
        public static string GetOrderByAscOrDescImage(int? OrderedBy)
        {
            var iconSort = string.Empty;
            switch (OrderedBy)
            {
                case 1:
                    iconSort = "down";
                    break;
                case 2:
                    iconSort = "up";
                    break;
                default:
                    iconSort = "down";
                    break;
            }
            return iconSort;
        }
        public static string GetReportType(int? type)
        {
            var service = GetService<Reports.Models.ReportType>();
            if (type == null || type == 0)
            {
                return "Ninguno";
            }
            var item = service.Get(type ?? 1);
            return item.Name;           
        }

        public static IEnumerable<Reports.Models.ReportType> GetReportTypesList()
        {
            var service = GetService<Reports.Models.ReportType>();
            return service.Get();
        }

        public static IEnumerable<Reports.Models.MessageSource> GetMessageSourceList()
        {
            var service = GetService<Reports.Models.MessageSource>();
            return service.Get();
        }

        public static IEnumerable<Reports.Models.Report> GetReportList()
        {
            var service = GetService<Reports.Models.Report>();
            return service.Get();
        }

        public static List<ReportElementData> GetReportSourceStatistics(IEnumerable<Reports.Models.MessageSource> sources, IEnumerable<Reports.Models.Report> reports )
        {
            var statisticsList = new List<ReportElementData>();
            if (reports.IsNotEmptyOrNull() && sources.IsNotEmptyOrNull())
            {
                foreach (var messageSource in sources)
                {
                    var newElementStatistics = new ReportElementData();
                    newElementStatistics.Count = reports.Count(a => a.SourceId == messageSource.Id);
                    if (newElementStatistics.Count > 0)
                    {
                        decimal percentageAsDecimal = (decimal)newElementStatistics.Count / (decimal)reports.Count();
                        var a = percentageAsDecimal * 100;
                        var roundedPercentage = System.Math.Round(a, 2, MidpointRounding.ToEven);
                        newElementStatistics.Percentage = roundedPercentage.ToString() + " %";
                    }
                    else
                    {
                        newElementStatistics.Percentage = "0 %";
                    }
                    newElementStatistics.Name = messageSource.Name;
                    statisticsList.Add(newElementStatistics);
                }
            }
            return statisticsList;
        }



        public static List<ReportElementData> GetReportTypesStatistics(IEnumerable<Reports.Models.ReportType> types, IEnumerable<Reports.Models.Report> reports)
        {
            var statisticsList = new List<ReportElementData>();
            if (reports.IsNotEmptyOrNull() && types.IsNotEmptyOrNull())
            {
                foreach (var rType in types)
                {
                    var newElementStatistics = new ReportElementData();
                    newElementStatistics.Count = reports.Count(a => a.Type == rType.Id);
                    if (newElementStatistics.Count > 0)
                    {
                        decimal percentageAsDecimal = (decimal)newElementStatistics.Count / (decimal)reports.Count();
                        var a = percentageAsDecimal * 100;
                        var roundedPercentage = System.Math.Round(a, 2, MidpointRounding.ToEven);
                        newElementStatistics.Percentage = roundedPercentage.ToString() + " %";
                    }
                    else
                    {
                        newElementStatistics.Percentage = "0 %";
                    }
                    newElementStatistics.Name = rType.Name;
                    statisticsList.Add(newElementStatistics);
                }
            }
            return statisticsList;
        }



        public static string GetReportTypeText(IEnumerable<Reports.Models.ReportType> rTypesList, int? rtype)
        {
            if (rtype.IsEmptyOrNull() || rtype == 0)
            {
                return "Ninguno";
            }
            return rTypesList.Where(a => a.Id == rtype).FirstOrDefault().Name;
        }

        public static string GetMessageSourceText(IEnumerable<Reports.Models.MessageSource> mSourceList, int? mSource )
        {
            if (mSource.IsEmptyOrNull() || mSource == 0)
            {
                return "Ninguno";
            }
            return mSourceList.Where(a => a.Id == mSource).FirstOrDefault().Name;
        }

        public static int GetReportTypeNone()
        {
            var service = GetService<Reports.Models.ReportType>();
            var items = service.Get();
            var item = items.Where(a => a.Name == "Ninguno").FirstOrDefault();
            return item.Id;
        }


        public static string GetSuggestionType(int? type)
        {
            var textToReturn = string.Empty;
            switch (type)
            {
                case null:
                    textToReturn = "Ninguna.";
                    break;
                default:
                    textToReturn = Reports.Models.Enums.SuggestionDictionary[(Reports.Models.Enums.SuggestionType)type];
                    break;
            }
            return textToReturn;
        }



        public static RestClient<T> GetService<T>()
        {
            var baseUri = SiteParameter.Get("reports.api").Get<string>();

            switch (typeof(T).Name)
            {
                case "SuggestionTypesService":
                    return new SuggestionTypesService(baseUri) as RestClient<T>;
                case "ReportType":
                    return new ReportTypesService(baseUri) as RestClient<T>;
                case "MessageSource":
                    return new MessageSourcesClient(baseUri) as RestClient<T>;
                case "InvolvedType":
                    return new InvolvedTypesServices(baseUri) as RestClient<T>;
                case "Report":
                    return new ReportService(baseUri) as RestClient<T>; 
                default:
                    throw new TypeInitializationException(typeof(T).Name, null);
            }
        }

        public static IEnumerable<SelectListItem> GetReportTypesAsSelectList()
        {
            var service = GetService<Reports.Models.ReportType>();
            var items = service.Get();

            return items.Select(i => new SelectListItem
            {
                Text = i.Name,
                Value = i.Id.ToString(CultureInfo.InvariantCulture),
            });
        }

        public static IEnumerable<SelectListItem> GetSuggestionTypesAsSelectList()
        {
            var service = GetService<Reports.Models.SuggestionType>();
            var items = service.Get();

            return items.Select(i => new SelectListItem
            {
                Text = i.Name,
                Value = i.Id.ToString(CultureInfo.InvariantCulture),
            });
        }


        public static IEnumerable<SelectListItem> GetMessageSourcesAsSelectList()
        {
            var service = GetService<Reports.Models.MessageSource>();
            var items = service.Get();

            return items.Select(i => new SelectListItem
            {
                Text = i.Name,
                Value = i.Id.ToString(CultureInfo.InvariantCulture),
            });
        }


        public static IEnumerable<SelectListItem> GetInvolvedTypesAsSelectList()
        {
            var service = GetService<Reports.Models.InvolvedType>();
            var items = service.Get();

            return items.Select(i => new SelectListItem
            {
                Text = i.Name,
                Value = i.Id.ToString(CultureInfo.InvariantCulture),
            });
        }


        public static InvolvedType GetInvolvedTypeById(int involvedTypeId)
        {
            var service = GetService<Reports.Models.InvolvedType>();
            var item = service.Get(involvedTypeId);

            return item.IsNotEmptyOrNull() ? item : new InvolvedType();
        }


        public static bool AbleToBasedOnStatus(int? status)
        {
            if (status != (int) Reports.Models.Enums.ReportsStatus.Closed)
                return true;
            return false;
        }


        public static string GetDisplayTimeZone()
        {
        //    CacheManager.Cache.Get<Paginable<T>>(new CacheRequest()
        //    {
        //        Key = MethodBase.GetCurrentMethod().Name + this.Rest.BaseUri + this.Resource + url,
        //        Callback = (Func<object>)(() => (object)this.Rest.GetResource<Paginable<T>>(this.Resource + StringUtils.FormatWith("/?request={0}", new object[1]
        //{
        //  (object) url
        //})))
        //    });
            var timeZoneId = SiteParameter.Get("reports.displayTimeZone").Get<string>();
            return timeZoneId.IsNotEmptyOrNull() ? timeZoneId : "UTC";
        }

        public static string GetUserShortName(int userId)
        {
            if (userId > 0)
            {
                var user = UserService.Manager.GetUser(userId);
                if (user.IsNotEmptyOrNull())
                {
                    return user.firstName + " " + user.lastName;
                }
                else
                {
                    return string.Empty;
                }
            }
            else
            {
                return string.Empty;
            }
        }

        public static Report GetNext(List<Report> list, Report current)
        {
            try
            {
                return list.SkipWhile(x => !x.Id.Equals(current.Id)).Skip(1).First();
            }
            catch
            {
                return default(Report);
            }
        }


        public static Report GetPrevious(List<Report> list, Report current)
        {
            try
            {
                return list.TakeWhile(x => !x.Id.Equals(current.Id)).Last();
            }
            catch
            {
                return default(Report);
            }
        }


        public static ViewReport CreateViewReport(Report current,Report next,Report previous)
        {
            return new ViewReport(current,next,previous);
        }

        public static ViewReport CreateViewReportModel(Report current,IEnumerable<Report> reportsList)
        {
            var next = GetNext(reportsList.ToList(), current);
            var prev = GetPrevious(reportsList.ToList(), current);
            return CreateViewReport(current,next,prev);
        }

    }
}