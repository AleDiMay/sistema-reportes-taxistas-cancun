﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Legendary.Web.Models;
using Reports.Models;
using UserService.Model;

namespace Reports.Client
{
    public class MessageSourcesClient : RestClient<MessageSource>
    {
        public MessageSourcesClient(string baseUri)
            : base(new RestServicesWithEncryptedAuthentication(baseUri))
      { }


        public override MessageSource Update(int id, MessageSource reportType)
        {
            throw new NotImplementedException("Update");
        }

        public override void Delete(int id)
        {
            throw new NotImplementedException("Delete");
        }


    }
}
