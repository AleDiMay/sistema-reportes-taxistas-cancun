﻿using System.ComponentModel.DataAnnotations;

namespace Reports.Models
{
    [MetadataType(typeof (ReportsMD))]
    public partial class Report
    {
    }

    public class ReportsMD
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "El email no puede estar vacío")]
        [EmailAddress(ErrorMessage = "El email no es válido")]
        [StringLength(50, ErrorMessage = "El correo no puede tener mas de 50 caracteres.")]
        public string Email { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "La ubicación del percance es necesaria")]
        public string IncidentLocation { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "La descripción del percance es necesaria")]
        [StringLength(3000, ErrorMessage = "El mensaje no puede tener mas de 3000 caracteres.")]
        public string Description { get; set; }

        [StringLength(100, ErrorMessage = "El mensaje no puede tener mas de 300 caracteres.")]
        public string DriverDescription { get; set; }

        [StringLength(20, ErrorMessage = "El telefono no puede tener más de 20 caracteres")]
        public string Phone { get; set; }

        [StringLength(50, ErrorMessage = "El número de documento no puede tener más de 50 caracteres")]
        public string DocumentReferenceNumber { get; set; }

        [StringLength(50, ErrorMessage = "El número de conductor no puede tener más de 50 caracteres")]
        public string DriverNumber { get; set; }

        [StringLength(10, ErrorMessage = "El número de placa no puede tener más de 10 caracteres")]
        public string NumberPlate { get; set; }

        [StringLength(50, ErrorMessage = "El nombre del afectado no puede tener más de 50 caracteres")]
        public string ComplainantName { get; set; }

        [StringLength(100, ErrorMessage = "El Twitter  no puede tener más de 100 caracteres")]
        public string Twitter { get; set; }

        [StringLength(100, ErrorMessage = "El Facebook  no puede tener más de 100 caracteres")]
        public string Facebook { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "El Titulo es Necesario")]
        [StringLength(50, ErrorMessage = "El Titulo  no puede tener más de 50 caracteres")]
        public string Title { get; set; }
    }
}