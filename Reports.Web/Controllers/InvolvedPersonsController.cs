﻿using System;
using System.Web.Mvc;
using CommonService;
using Legendary.Core.Utils;
using Reports.Client;
using Reports.Models;

namespace Reports.Application.Controllers
{
    public class InvolvedPersonsController : Controller
    {
        private readonly GeneralReportServices _generalReportServices;
        // GET: InvolvedPersons

        public InvolvedPersonsController()
        {
            _generalReportServices = new GeneralReportServices(SiteParameter.Get("reports.api").Get<string>());
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Create(int reportId)
        {
            var reportFrom = _generalReportServices.GetReport(reportId);
            if (reportFrom.IsNotEmptyOrNull())
            {
                var involvedPerson = new InvolvedPerson() { ReportId = reportId };
                return View(involvedPerson);                
            }
            return RedirectToAction("Index", "Reports");
        }

        [HttpPost]
        public ActionResult Create(InvolvedPerson involvedPerson)
        {
            var todayDate = new DateTime();
            todayDate = TimeZoneInfo.ConvertTimeToUtc(DateTime.Now, TimeZoneInfo.Local);

            involvedPerson.CreatedByUserId = UserService.LoginManager.GetLoggedUser().userId;
            involvedPerson.UtcDateCreated = todayDate;

            if (!ModelState.IsValid)
                return View(involvedPerson);

            _generalReportServices.CreateInvolvedPerson(involvedPerson);
            return RedirectToAction("Details", "Reports", new { reportId = involvedPerson.ReportId });
        }

    }
}