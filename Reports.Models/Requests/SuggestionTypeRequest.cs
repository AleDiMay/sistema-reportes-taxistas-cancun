﻿using System.Linq;
using Legendary.Core.Model.Request;
using Legendary.Core.Utils;

namespace Reports.Models.Requests
{
    public class SuggestionTypeRequest : PaginatedRequest<SuggestionType>
    {
        public int Id { get; set; }

        public override IQueryable<SuggestionType> ExecuteRequest(IQueryable<SuggestionType> original)
        {
            if (Id.IsNotEmptyOrNull() && Id > 0)
                original = original.Where(c => c.Id == Id);
            return original;
        }
    }
}