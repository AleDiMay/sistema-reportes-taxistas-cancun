﻿using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;
using Legendary.Core.Model;
using Legendary.Core.Utils;
using Reports.Models;
using Reports.Models.Requests;
using Enums = Legendary.Core.Model.Enums;
using EntityState = System.Data.Entity.EntityState;
using System.Collections.Generic;

namespace Reports.API.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class ReportsController : BaseController
    {
        // GET: api/Reports
        [UserService.EncryptedApiAuthorization((int) SecurityService.Model.SystemPermissions.ReportsPermissions.ViewReport)]
        public IQueryable<Report> GetReports()
        {
            return db.Reports.OrderByDescending(x => x.UtcDateCreated);
        }


        // GET: api/Reports
        [UserService.EncryptedApiAuthorization((int) SecurityService.Model.SystemPermissions.ReportsPermissions.ViewReport)]
        [ResponseType(typeof (Paginable<Report>))]
        public async Task<IHttpActionResult> GetReports(string request)
        {
            try
            {
                var finalRequest = request.UrlDecode<ReportsRequest>();
                var types = db.Reports.OrderByDescending(x => x.UtcDateCreated);
                var results = await finalRequest.ExecuteRequestAsync(types);
                var paginated = results.Paginate(finalRequest);
                return Ok(paginated);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        // GET: api/Reports/5
        [ResponseType(typeof (Report))]
        [UserService.EncryptedApiAuthorization((int) SecurityService.Model.SystemPermissions.ReportsPermissions.ViewReport)]
        public async Task<IHttpActionResult> GetReport(int id)
        {
            Report report = await db.Reports
                .Include(r => r.MessageSource)
                .Include(r => r.Editions)
                .Include(r => r.InvolvedPersons)
                .FirstOrDefaultAsync(r => r.Id == id);

            if (report == null)
            {
                return NotFound();
            }
            report.Title = db.Reports.FirstOrDefaultAsync(r => r.Id == id).Result.Title;
            report.Priority = db.Reports.FirstOrDefaultAsync(r => r.Id == id).Result.Priority;
            report.UtcDateExpiration = db.Reports.FirstOrDefaultAsync(r => r.Id == id).Result.UtcDateExpiration;
            return Ok(report);
        }


        // PUT: api/Reports/5
        [ResponseType(typeof (void))]
        [UserService.EncryptedApiAuthorization((int) SecurityService.Model.SystemPermissions.ReportsPermissions.EditReport)]
        public async Task<IHttpActionResult> PutReport(int id, Report report)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != report.Id)
            {
                return BadRequest();
            }

            db.Entry(report).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ReportExists(id))
                {
                    return NotFound();
                }
                throw;
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Reports
        [ResponseType(typeof (Report))]
        //*[UserService.EncryptedApiAuthorization((int)SecurityService.Model.SystemPermissions.ReportsPermissions.CreateReport)]
        public async Task<IHttpActionResult> PostReport(Report report)
        {
            // "Central Standard Time (Mexico)"
            var todayDate = new DateTime();
            todayDate = TimeZoneInfo.ConvertTimeToUtc(DateTime.Now, TimeZoneInfo.Local);
            report.UtcDateCreated = todayDate;
            if (report.Priority.IsEmptyOrNull())
            {
                report.Priority = 2;
            }
            if (report.Title.IsEmptyOrNull())
            {
                report.Title = "Nuevo reporte desde Pagina Web";
            }
            if (report.UtcDateExpiration.IsEmptyOrNull())
            {
                report.UtcDateExpiration = DateTime.Now.AddDays(15);
            }
            report.UtcDateModified = report.UtcDateCreated;
            //TimeZoneInfo cstZone = TimeZoneInfo.FindSystemTimeZoneById("Central Standard Time");
            //DateTime cstTime = TimeZoneInfo.ConvertTimeFromUtc(todayDate, cstZone);


            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Reports.Add(report);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new {id = report.Id}, report);
        }

        // DELETE: api/Reports/5
        [ResponseType(typeof (Report))]
        [UserService.EncryptedApiAuthorization((int) SecurityService.Model.SystemPermissions.ReportsPermissions.DeleteReport)]
        public async Task<IHttpActionResult> DeleteReport(int id)
        {
            Report report = await db.Reports.FindAsync(id);
            if (report == null)
            {
                return NotFound();
            }

            db.Reports.Remove(report);
            await db.SaveChangesAsync();

            return Ok(report);
        }

        [ResponseType(typeof (Report))]
        public IHttpActionResult Options()
        {
            return Ok(new Report());
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ReportExists(int id)
        {
            return db.Reports.Count(e => e.Id == id) > 0;
        }
    }
}