﻿using System.Linq;
using Legendary.Core.Model.Request;
using Legendary.Core.Utils;

namespace Reports.Models.Requests
{
    public class ReportTypeRequest : PaginatedRequest<ReportType>
    {
        public int Id { get; set; }

        public override IQueryable<ReportType> ExecuteRequest(IQueryable<ReportType> original)
        {
            if (Id.IsNotEmptyOrNull() && Id > 0)
                original = original.Where(c => c.Id == Id).OrderByDescending(x => x.Id);
            return original;
        }
    }
}