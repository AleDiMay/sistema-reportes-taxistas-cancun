﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Legendary.Core.Utils;
using Reports.Models;
using Reports.Models.Requests;

namespace Reports.Application.Models
{
    public class ViewModels
    {
        public class EmailResponse
        {
            [Required]
            public int ReportId { get; set; }
            [Required]
            public string AddressTo { get; set; }
            [Required]
            public string Body { get; set; }

        }
    }

    public class PaginatedReports
    {
        public PaginatedReports()
        {
        }

        public PaginatedReports(IEnumerable<Reports.Models.Report> reports, long selectedPage, long totalPages, long totalItems, ReportsRequest request)
        {
            Reports = reports;
            SelectedPage = selectedPage;
            TotalPages = totalPages;
            TotalItems = totalItems;
            Request = request;
        }

        public IEnumerable<Reports.Models.Report> Reports { get; set; }
        public long SelectedPage { get; set; }
        public long TotalPages { get; set; }
        public long TotalItems { get; set; }
        public ReportsRequest Request { get; set; }
    }

    public class ReportElementData
    {
        public string Name { get; set; }
        public int Count { get; set; }
        public string Percentage { get; set; }
    }

    public class ReportsStatistics
    {
        public int TotalReports { get; set; }
        public string TotalReportsText { get; set; }
        public List<ReportElementData> ReportSourcesStatistics { get; set; }
        public List<ReportElementData> ReportTypesStatistics { get; set; }
        public ReportsRequest ReportsRequest { get; set; }

    }


    public class ViewReport
    {
        public ViewReport(Report c,Report n,Report p)
        {
            CurrentReport = c;
            PreviousReportId = p.IsNotEmptyOrNull() ? p.Id : 0;
            NextReportId = n.IsNotEmptyOrNull() ? n.Id : 0;

        }

        public Report CurrentReport { get; set; }

        public int? PreviousReportId { get; set; }

        public int? NextReportId { get; set; }

    }


}