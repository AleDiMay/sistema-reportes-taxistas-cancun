﻿using System;
using System.Web.Mvc;
using CommonService;
using Legendary.Core.Utils;
using Reports.Application.Models;
using Reports.Client;
using Reports.Models;

namespace Reports.Application.Controllers
{
    public class EditionsController : BaseLoggedController
    {
        private readonly GeneralReportServices _generalReportServices;

        public EditionsController()
        {
            _generalReportServices = new GeneralReportServices(SiteParameter.Get("reports.api").Get<string>());
        }

        public JsonResult SaveResponseEmailed(ViewModels.EmailResponse emailResponse)
        {
            var done = false;
            if (!ModelState.IsValid)
            {
                done = false;
            }

            var report = _generalReportServices.GetReport(emailResponse.ReportId);
            if (report.IsNotEmptyOrNull())
            {
                done = true;
                var todayDate = new DateTime();
                todayDate = TimeZoneInfo.ConvertTimeToUtc(DateTime.Now, TimeZoneInfo.Local);
                var edition = new Edition();
                edition.ReportId = emailResponse.ReportId;
                edition.UTCDateEdited = todayDate;
                edition.EditedById = UserService.LoginManager.GetLoggedUser().userId;
                edition.Content = "Email de respuesta enviado : " + emailResponse.Body;

                _generalReportServices.CreateEdition(edition);

                report.Status = (int) Enums.ReportsStatus.InFollowing;
                report.ModifiedByUserId = UserService.LoginManager.GetLoggedUser().userId;
                report.UtcDateModified = todayDate;
                _generalReportServices.UpdateReport(report);
            }

            return new JsonResult() { JsonRequestBehavior = JsonRequestBehavior.AllowGet, Data = new { saved = done } };
        }

        public ActionResult Create(int reportId)
        {
            var edition = new Edition { ReportId = reportId };
            return View(edition);
        }

        [HttpPost]
        public ActionResult Create(Edition edition)
        {
            // "Central Standard Time (Mexico)"
            var todayDate = TimeZoneInfo.ConvertTimeToUtc(DateTime.Now, TimeZoneInfo.Local);
            edition.UTCDateEdited = todayDate;
            edition.EditedById = UserService.LoginManager.GetLoggedUser().userId;

            if (!ModelState.IsValid)
                return View(edition);

            var report = _generalReportServices.GetReport(edition.ReportId);
            if (report.IsEmptyOrNull())
                return RedirectToAction("Index", "Reports");
            if (report.Status != (int)Enums.ReportsStatus.Closed)
            {
                report.ModifiedByUserId = edition.EditedById;
                report.Status = (int)Enums.ReportsStatus.InFollowing;
                try
                {
                    _generalReportServices.UpdateReport(report);
                    _generalReportServices.CreateEdition(edition);

                }
                catch (Exception)
                {
                    return RedirectToAction("Index", "Reports");
                }                
            }

            return RedirectToAction("Details", "Reports", new { reportId = edition.ReportId });
        }

        public ActionResult Close(int reportId)
        {
            var edition = new Edition { ReportId = reportId };
            return View(edition);
        }

        [HttpPost]
        public ActionResult Close(Edition edition)
        {
            // "Central Standard Time (Mexico)"
            var todayDate = new DateTime();
            todayDate = TimeZoneInfo.ConvertTimeToUtc(DateTime.Now, TimeZoneInfo.Local);
            edition.UTCDateEdited = todayDate;
            edition.EditedById = UserService.LoginManager.GetLoggedUser().userId;


            if (!ModelState.IsValid)
                return View(edition);

            var report = _generalReportServices.GetReport(edition.ReportId);
            if (report.IsEmptyOrNull())
                return RedirectToAction("Index", "Reports");
            if (report.Status != (int)Enums.ReportsStatus.Closed)
            {
                report.ModifiedByUserId = edition.EditedById;
                report.Status = (int)Enums.ReportsStatus.Closed;
                report.Priority = (int)Enums.ReportPriority.Low;
                try
                {
                    _generalReportServices.UpdateReport(report);
                    _generalReportServices.CreateEdition(edition);

                }
                catch (Exception)
                {
                    return RedirectToAction("Index", "Reports");
                }
            }

            return RedirectToAction("Details", "Reports", new { reportId = edition.ReportId });
        }
        [HttpPost]
        public ActionResult changePriority(int reportId, int Priority)
        {
            String PriorityText = string.Empty;
            switch (Priority)
            {
                case 0:
                    PriorityText = "Urgente";
                    break;
                case 1:
                    PriorityText = "Baja";
                    break;
                case 2:
                    PriorityText = "Regular";
                    break;
                case 3:
                    PriorityText = "Baja";
                    break;                    
            }
            var edition = new Edition { ReportId = reportId };
            var todayDate = new DateTime();
            todayDate = TimeZoneInfo.ConvertTimeToUtc(DateTime.Now, TimeZoneInfo.Local);
            edition.UTCDateEdited = todayDate;
            edition.EditedById = UserService.LoginManager.GetLoggedUser().userId;


            if (!ModelState.IsValid)
                return View(edition);

            var report = _generalReportServices.GetReport(reportId);
            if (report.IsEmptyOrNull())
                return RedirectToAction("Index", "Reports");
            if (report.Status != (int)Enums.ReportsStatus.Closed)
            {
                report.ModifiedByUserId = edition.EditedById;
                report.Priority = Priority;
                report.UtcDateModified = todayDate;
                edition.Content = "Prioridad del reporte actualizada a " + PriorityText;
                try
                {
                    _generalReportServices.UpdateReport(report);
                    _generalReportServices.CreateEdition(edition);

                }
                catch (Exception)
                {
                    return RedirectToAction("Index", "Reports");
                }
            }
            return RedirectToAction("Details", "Reports", new { reportId = edition.ReportId });
        }
       
    }
}