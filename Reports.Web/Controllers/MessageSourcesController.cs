﻿using System.Web.Mvc;
using CommonService;
using Reports.Client;
using Reports.Models;

namespace Reports.Application.Controllers
{
    public class MessageSourcesController : BaseLoggedController
    {
        private readonly GeneralReportServices _generalReportServices;
        public MessageSourcesController()
        {
            _generalReportServices = new GeneralReportServices(SiteParameter.Get("reports.api").Get<string>());
        }

        // GET: MessageSources
        public ActionResult Index()
        {
            var messageResources = _generalReportServices.GetMessageSources();
            return View(messageResources);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(MessageSource messageSource)
        {
            if (!ModelState.IsValid)
                return View(messageSource);


            _generalReportServices.CreateMessageSource(messageSource);
            return RedirectToAction("Index", "MessageSources");
        }

    }
}