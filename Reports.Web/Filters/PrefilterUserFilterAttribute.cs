﻿using System;
using System.Linq;
using System.Web.Mvc;
using System.Web.Routing;
using Legendary.Core.Utils;
using Reports.Application.Helpers;
using Reports.Models;

namespace Reports.Application.Filters
{
    public class PrefilterUserFilterAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (filterContext == null)
                return;

            const string key = "userid";
            var containsParam = filterContext.ActionParameters.ContainsKey(key)
               || filterContext.HttpContext.Request.Params.AllKeys.Any(k => k.Equals(key, StringComparison.InvariantCultureIgnoreCase));

            if (!containsParam)
                return;

            try
            {
                var userId = filterContext.ActionParameters.ContainsKey(key)
                   ? filterContext.ActionParameters[key].GetOrDefault<int>()
                   : filterContext.HttpContext.Request[key].Get<int>();

                SiteOptions.PrefilterUserId(ref userId);

                if (userId <= 0)
                    filterContext.Result = new RedirectToRouteResult(
                       new RouteValueDictionary { { "controller", "Home" }, { "action", "Index" } });
            }
            catch
            {

            }
        }
    }
}