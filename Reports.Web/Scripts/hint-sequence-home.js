﻿var enjoyhint_instance = new EnjoyHint({});

var enjoyhint_script_steps = [
  {
      'next .navbar-brand': 'Bienvenido a la nueva versión de Vigía Ciudadano.<br>A traves de este breve tutorial te enseñaremos algunas nuevas características.',
      'nextButton': { className: "myNext", text: "Continuar" },
      'skipButton': { className: "mySkip", text: "Saltar" }
  },
  {
      'next .open-filter': 'El sistema de <b>Filtrado</b> es útil para encontrar un registro o un grupo de registros en específico.',
      'nextButton': { className: "myNext", text: "Continuar" },
      'showSkip': false
  },
  {
      'next .reportunit': 'Cada fila representa un reporte, los marcados en <b>Negritas</b> son reportes aún sin leer.',
      'nextButton': { className: "myNext", text: "Continuar" },
      'showSkip': false
  },
  {
      'next .priority': 'Ahora los reportes incluyen "Prioridad"<br>Una nueva forma de darle especial importancia a ciertos Reportes<br>Entre las prioridades que podras encontrar se encuentran:<br><ul><li>Urgente</li><li>Alta</li><li>Regular</li><li>Baja</li>',
      'nextButton': { className: "myNext", text: "Continuar" },
      'showSkip': false
  },
  {
      'next .createnew': 'Usa este boton para crear un nuevo reporte.',
      'nextButton': { className: "myNext", text: "Continuar" },
      'showSkip': false
  },
  {
      'next .navbar-brand': 'Esta nueva presentación tiene el fin de hacer mas faciles y amigables los procesos de esta aplicación.<br>Si tienes alguna duda sobre su uso o la ubicación de ciertos datos, comunicate al departamento de desarrollo en la extensión <b>2603</b>',
      'nextButton': { className: "myNext", text: "Terminar" },
      'showSkip': false
  }

];

enjoyhint_instance.set(enjoyhint_script_steps);