﻿using System.ComponentModel.DataAnnotations;

namespace Reports.Models
{
    [MetadataType(typeof (EditionMD))]
    public partial class Edition
    {
    }

    public class EditionMD
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "Un mensaje de contenido es necesario")]
        [StringLength(3000, ErrorMessage = "El mensaje no puede tener mas de 3000 caracteres.")]
        public string Content { get; set; }

        [Required(ErrorMessage = "El enlace con el reporte es necesario")]
        public int ReportId { get; set; }
    }
}