﻿using System.ComponentModel.DataAnnotations;

namespace Reports.Models
{
    [MetadataType(typeof (MessageSourceMd))]
    public partial class MessageSource
    {
    }

    public class MessageSourceMd
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "Un nombre de origen es necesario")]
        public string Name { get; set; }
    }
}