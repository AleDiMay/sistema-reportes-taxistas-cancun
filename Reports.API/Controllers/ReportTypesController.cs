﻿using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Reports.Models;

namespace Reports.API.Controllers
{
    public class ReportTypesController : BaseController
    {
        [UserService.EncryptedApiAuthorization(
            (int) SecurityService.Model.SystemPermissions.ReportsPermissions.ViewReportType)]
        public IQueryable<ReportType> GetReportTypes()
        {
            return db.ReportTypes;
        }


        // GET: api/Reports/5
        [UserService.EncryptedApiAuthorization(
            (int) SecurityService.Model.SystemPermissions.ReportsPermissions.ViewReportType)]
        [ResponseType(typeof (ReportType))]
        public async Task<IHttpActionResult> GetReportType(int id)
        {
            ReportType reportType = await db.ReportTypes.FindAsync(id);
            if (reportType == null)
            {
                return NotFound();
            }

            return Ok(reportType);
        }


        // PUT: api/Reports/5
        [UserService.EncryptedApiAuthorization(
            (int) SecurityService.Model.SystemPermissions.ReportsPermissions.EditReportType)]
        [ResponseType(typeof (void))]
        public async Task<IHttpActionResult> PutReportType(int id, ReportType reportType)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != reportType.Id)
            {
                return BadRequest();
            }

            db.Entry(reportType).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ReportTypeExists(id))
                {
                    return NotFound();
                }
                throw;
            }

            return StatusCode(HttpStatusCode.NoContent);
        }


        // POST: api/Reports
        [UserService.EncryptedApiAuthorization(
            (int) SecurityService.Model.SystemPermissions.ReportsPermissions.CreateReportType)]
        [ResponseType(typeof (ReportType))]
        public async Task<IHttpActionResult> PostReportType(ReportType reportType)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.ReportTypes.Add(reportType);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new {id = reportType.Id}, reportType);
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ReportTypeExists(int id)
        {
            return db.ReportTypes.Count(e => e.Id == id) > 0;
        }
    }
}