﻿using System.Linq;
using Legendary.Core.Model.Request;
using Legendary.Core.Utils;

namespace Reports.Models.Requests
{
    public class AllocationDepartmentUserRequest : PaginatedRequest<AllocationDepartmentUser>
    {
        public int? AllocationDepartmentId { get; set; }

        public int? UserId { get; set; }

        public override IQueryable<AllocationDepartmentUser> ExecuteRequest(
            IQueryable<AllocationDepartmentUser> original)
        {
            if (AllocationDepartmentId.IsNotEmptyOrNull() && AllocationDepartmentId > 0)
                original = original.Where(c => c.AllocationDepartmentId == AllocationDepartmentId);
            if (UserId.IsNotEmptyOrNull() && UserId > 0)
                original = original.Where(c => c.UserId == UserId);

            return original;
        }
    }
}