﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Legendary.Core.Utils;
using Legendary.Web.Models;
using Reports.Models;
using UserService.Model;

namespace Reports.Client
{
    public class ReportTypesService : RestClient<ReportType>
    {
        //private readonly RestServicesWithAuthentication _rest;

        public ReportTypesService(string baseUri)
            : base(new RestServicesWithEncryptedAuthentication(baseUri))
      { }

        public override ReportType Update(int id, ReportType reportType)
        {
            throw new NotImplementedException("Update");
        }

        public override void Delete(int id)
        {
            throw new NotImplementedException("Delete");
        }

    }
}
