﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reports.Models
{
    [MetadataType(typeof(ReportsMD))]
    public partial class Report
    {
    }

    public class ReportsMD
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "El email no puede estar vacío")]
        [EmailAddress(ErrorMessage = "El email no es válido")]
        [StringLength(50, ErrorMessage = "El correo no puede tener mas de 50 caracteres.")]
        public string Email { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "La ubicación del percance es necesaria")]
        public string IncidentLocation { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "La descripción del percance es necesaria")]
        [StringLength(3000, ErrorMessage = "El mensaje no puede tener mas de 3000 caracteres.")]
        public string Description { get; set; }

        [StringLength(100, ErrorMessage = "El mensaje no puede tener mas de 300 caracteres.")]
        public string DriverDescription { get; set; }

        [StringLength(20, ErrorMessage = "El telefono no puede tener más de 20 caracteres")]
        public string Phone { get; set; }

        [StringLength(50, ErrorMessage = "El número de documento no puede tener más de 50 caracteres")]
        public string DocumentReferenceNumber { get; set; }

        [StringLength(50, ErrorMessage = "El número de conductor no puede tener más de 50 caracteres")]
        public string DriverNumber { get; set; }

        [StringLength(10, ErrorMessage = "El número de placa no puede tener más de 10 caracteres")]
        public string NumberPlate { get; set; }

        [StringLength(50, ErrorMessage = "El nombre del afectado no puede tener más de 50 caracteres")]
        public string ComplainantName { get; set; }

        [StringLength(100, ErrorMessage = "El Twitter  no puede tener más de 100 caracteres")]
        public string Twitter { get; set; }

        [StringLength(100, ErrorMessage = "El Facebook  no puede tener más de 100 caracteres")]
        public string Facebook { get; set; }

    }

    [MetadataType(typeof(SuggestionMD))]
    public partial class Suggestion
    {
    }

    public class SuggestionMD
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "El email no puede estar vacío")]
        [EmailAddress(ErrorMessage = "El email no es válido")]
        public string Email { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "El nombre es necesario")]
        public string Name { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Un mensaje es necesario")]
        [StringLength(1200, ErrorMessage = "El mensaje no puede tener mas de 1200 caracteres.")]
        public string Description { get; set; }
        //[MetadataType(typeof(SuggestionMD))]
    }

    [MetadataType(typeof(EditionMD))]
    public partial class Edition
    {
    }

    public partial class EditionMD
    {

        [Required(AllowEmptyStrings = false, ErrorMessage = "Un mensaje de contenido es necesario")]
        [StringLength(3000, ErrorMessage = "El mensaje no puede tener mas de 3000 caracteres.")]
        public string Content { get; set; }

        [Required(ErrorMessage = "El enlace con el reporte es necesario")]
        public int ReportId { get; set; }
    }

    [MetadataType(typeof(MessageSourceMd))]
    public partial class MessageSource
    {

    }

    public partial class MessageSourceMd
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "Un nombre de origen es necesario")]
        public string Name { get; set; }         
    }


    [MetadataType(typeof(ReportTypeMd))]
    public partial class ReportType
    {

    }

    public partial class ReportTypeMd
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "Un nombre para el tipo de reporte es necesario")]
        public string Name { get; set; }
    }

    [MetadataType(typeof(InvolvedPersonMd))]
    public partial class InvolvedPerson
    {

    }


    public partial class InvolvedPersonMd
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "El nombre de la persona involucrada es necesario")]
        public string Name { get; set; }


        [Required(AllowEmptyStrings = false, ErrorMessage = "Es necesario ingresar un número de telefono")]
        public string Phone { get; set; }


    }


    [MetadataType(typeof(InvolvedTypeMd))]
    public partial class InvolvedType
    {

    }


    public partial class InvolvedTypeMd
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "Un nombre es requerido")]
        public string Name { get; set; }

    }

}
